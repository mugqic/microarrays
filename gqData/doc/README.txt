

####### Microarray probe annotations #########

microarray_annotations_library/data contains R objects that store the probe annotations for various platforms. Essentially,
these are data.frames with probes as rows and fvarLabels as columns. Three columns are mandatory and have been manually
added from the original source: 
	- ProbeID : unique identifier for a probe. Will match the identifier coming out of the scanner/analysis software.
	- SYMBOL  : biologist-interpretable symbol for the probed feature. Typically gene symbol. SYMBOL cannot be empty.
			ProbeID should replace empty symbols.
	- ENTREZID: NCBI ENTREZ gene identifier. This will be mainly be used to join with gene sets/pathways
	- rownames() should be set to ProbeID
        - Control probes should be set as attributes for later use

Typically, probe annotations are downloaded from GEO. Preference will be given to GPLs directly submitted by the 
manufacturer. Otherwise, probe annotations are downloaded from the manufacturer's website and editied in excel before saving
as an R object.

The name of the R object should be the platform.alias as defined in the package gqMicroarrays

inst/microarray_annotations_library_sources contains a trace file for the construction of the probe annotations.


########### Gene sets ##########################

Coming Soon


