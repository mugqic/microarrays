

#' This function returns a character vector listing which reference sequences (genomes) are packaged in gqData
#'
#' @title List what genomes are packaged in gqData
#' @export
getSupportedReferences <- function()
{
	basename(
		list.dirs(
			system.file(file.path('extdata','reference_sequences'),package='gqData')
	,recursive=FALSE)
	)
}




