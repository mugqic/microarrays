library(GEOquery)
library("org.Hs.eg.db")
options(stringsAsFactors=FALSE)

## Mappings useful for some platforms
x.human <- org.Hs.egSYMBOL2EG   # For the reverse map:
mapped_genes.human <- mappedkeys(x.human) # Get the entrez gene identifiers that are mapped to a gene symbol 
xx.human <- as.list(x.human[mapped_genes.human]) # Convert to a list



## Agilent-028005 SurePrint G3 Mouse GE 8x60K Microarray Sat  7 Jul 06:27:30 2012 
gpl = 'GPL10787'
fdata  = Table(getGEO(gpl))
fdata = data.frame(
	 'ProbeID'     = fdata[['ID']]
	,'SYMBOL'      = fdata[['GENE_SYMBOL']] 
	,'ENTREZID'    = fdata[['GENE']]
	,'Control'     = fdata[['CONTROL_TYPE']]
	,'Chromosome'  = gsub(':.*','',fdata[['CHROMOSOMAL_LOCATION']])
	,fdata,row.names=fdata$ID)
fdata$SYMBOL[fdata$SYMBOL==''] = NA
fdata$SYMBOL[is.na(fdata$SYMBOL)] = fdata$ProbeID[is.na(fdata$SYMBOL)] # replace emtpy probes
fdata$ENTREZID[fdata$ENTREZID==''] = NA # set emptpy entrez id as NA
fdata[['GO_ID']] = NULL # this is useless anyway
save(fdata,file='../../data/microarray_annotations_library/Agilent028005.RData')




## Illumina 450k Sat  7 Jul 06:27:38 2012 
gpl = 'GPL13534'
temp  = Table(getGEO(gpl))

fdata= temp
fdata = data.frame(
	 'ProbeID'     = fdata[['ID']]
	,'SYMBOL'      = sapply(strsplit(fdata$UCSC_RefGene_Name,split=';'),function(s) paste( unique(s),collapse=';' ) ) 
	,'ENTREZID'    = ''
	,'Control'     = ''
	,'Chromosome'  = fdata[['CHR']]
	,fdata,row.names=fdata$ID)
fdata$SYMBOL[fdata$SYMBOL==''] = NA
fdata$SYMBOL[is.na(fdata$SYMBOL)] = fdata$ProbeID[is.na(fdata$SYMBOL)] # replace emtpy probes
fdata$ENTREZID = mclapply(fdata$SYMBOL,function(s)
{
	s = unique(unlist(  strsplit(s,split=';')   ))
	paste(unlist(xx.human[match(s,names(xx.human))]),collapse=';')# hash ref like this is slow
},mc.cores=4)
fdata$ENTREZID = as.character(unlist(fdata$ENTREZID))
fdata$ENTREZID[fdata$ENTREZID==''] = NA
save(fdata,file='../../data/microarray_annotations_library/IlluminaHumanMethylation450.RData')



# Affy miRNA Mon  9 Jul 13:42:03 2012 
gpl = 'GPL14613'
temp  = Table(getGEO(gpl))

fdata= temp
fdata = data.frame(
	 'ProbeID'     = fdata[['ID']]
	,'SYMBOL'      = fdata[['Transcript ID(Array Design)']] 
	,'ENTREZID'    = ''
	,'Control'     = ''
	,'Chromosome'  = gsub(':.*','',fdata[['Alignments']])
	,fdata,row.names=fdata$ID)


fdata$SYMBOL[fdata$SYMBOL==''] = NA
fdata$SYMBOL[is.na(fdata$SYMBOL)] = fdata$ProbeID[is.na(fdata$SYMBOL)] # replace emtpy probes
save(fdata,file='../../data/microarray_annotations_library/Affymetrix_miRNA-2.0.RData')
# TODO:  see how the packages microRNA, miRNApath and Rmir can help you do pathway analysis











