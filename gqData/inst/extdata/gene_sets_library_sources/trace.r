# Statistics:
# GSEA,CAMERA,DAVIDS-EASE,grahite

# MSigDB, GeneSigDB, KEGG, IPA, GeneSigDB
# SPIA, topGO, GSVA, graphite
# rols, ramigo, 



options(stringsAsFactors=FALSE)
library(gqMicroarrays)


###
##### Ingenuity Canonical Pathways
###

# IPA data bases Mon  9 Jul 21:55:09 2012 
ipa = read.csv('IngenuityIdList.csv')

# IPA human
gene.sets  = data.frame(
	'GeneSetID'=paste(ipa[['IPA.ID']],'_Hs',sep='')
       ,'Name'=ipa[['PathwayName']]
       ,'Members'= ipa[['Entrez.Hs']]
       ,ipa)# Mandatory columns: GeneSetID, Members, Note

gene.sets$Members = strsplit(gene.sets$Members,split=', ')
gene.sets = gene.sets[,1:7]
min(sapply(gene.sets$Members,length))
rownames(gene.sets) = gene.sets$GeneSetID
save(gene.sets,file='../../data/ipa_human.RData')

# IPA mouse
gene.sets  = data.frame(
	'GeneSetID'=paste(ipa[['IPA.ID']],'_Mm',sep='')
       ,'Name'=ipa[['PathwayName']]
       ,'Members'= ipa[['Entrez.S1']]
       ,ipa)# Mandatory columns: GeneSetID, Members, Note

gene.sets$Members = strsplit(gene.sets$Members,split=', ')
gene.sets = gene.sets[,1:7]
min(sapply(gene.sets$Members,length))
rownames(gene.sets) = gene.sets$GeneSetID
save(gene.sets,file='../../data/ipa_mouse.RData')

# IPA Rat
gene.sets  = data.frame(
	'GeneSetID'=paste(ipa[['IPA.ID']],'_Rn',sep='')
       ,'Name'=ipa[['PathwayName']]
       ,'Members'= ipa[['Entrez.S2']]
       ,ipa)# Mandatory columns: GeneSetID, Members, Note

gene.sets$Members = strsplit(gene.sets$Members,split=', ')
gene.sets = gene.sets[,1:7]
min(sapply(gene.sets$Members,length))
rownames(gene.sets) = gene.sets$GeneSetID
save(gene.sets,file='../../data/ipa_rat.RData')




##
#### GeneSigDB
##
url="http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/msigdb.v4.0.entrez.gmt"
system(paste0("wget ",url))
gene.sets = get.gmt("msigdb.v4.0.entrez.gmt")
rownames(gene.sets) = paste0('MSigDB_',rownames(gene.sets))
gene.sets[['Name']] = gene.sets[['GeneSetID']]
gene.sets[['GeneSetID']]=  rownames(gene.sets)
gene.sets = gene.sets[,c("GeneSetID","URL", "Name" ,"Members")]
gene.sets$Members = strsplit(gene.sets$Members,split=',')
min(sapply(gene.sets$Members,length))
gene.sets$URL = sapply(gene.sets$URL,function(u){hwrite('LINK',link=u)})
save(gene.sets,file='../../../data/msigdb.RData')







