# This file documents how the various reference/genome sequences, indices and annotations were retrived and integrated to gqData


##
### hg19
##

## Genome Fasta, bowtie2 indices and annotations sources
# downloaded Fri 14 Sep 10:43:52 2012 from ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/UCSC/hg19/Homo_sapiens_UCSC_hg19.tar.gz

## Renaming to reference
# names were changed to reference.*. Don't like genome.*, not general enough

## files placed in hg19/

# fasta, bowtie index, annnotations, what else? Anything else for QC/diagnostics?


# The pipeline will copy the entire content of the folder to reference/.. 




