


#' Initialize a new project folder by creating directories and copying some necessary files.
#' 
#' This function creates a data folder to the working directory and populates it with template files. Most
#' importantly, it creates the params.txt file and pdata.txt file which need editing.
#'
#' @title Create a project folder
#' @param path path to where the skeleton of the db should be created
#' @param create.full.structure Logical. Should a full project structure be created also: docs,bin, etc. 
#' @author Lefebvre F
project.skeleton <- function(db.path, create.full.structure=FALSE)
{
	paths = db.path
	if(create.full.structure)
	{
		paths = c(paths,'bin','doc','deliverables') 
		file.copy(system.file(file.path('templates','README.txt')
			,package = "gqMicroarrays"),'.',overwrite=FALSE)# copy the README.txt	 
	}	
	sapply( dirs[!file.exists(paths)] , dir.create )
	
	# Copy necessary template files to db.path
	file.copy(list.files(system.file(file.path("templates",'db'),package = "gqMicroarrays"),full.names=TRUE)
			, db.path, overwrite=FALSE, recursive=TRUE)

	cat('\n\n\n*****\nTemplates copied. Fill out param.txt and pdata.txt\n\n*****\n\n\n')		
}



# this function read the chip, execultes multiple checks
project.build.db <-function(path)
{
	# read the params
	# do some checks, e.g. is varaible in bla bla.
	# don't forget to copy the trace	
}


#' Get the list of supported uA platforms
#' 
#' This function returns a list of uA platforms supported by the pipeline.
#' @title Get a template trace file
#' @author Lefebvre F
getSupportedMicroarrayTypes <- function()
{
	c('miRNA_Agilent','expression_Illumina')	
}











