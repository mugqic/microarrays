Overview
The main idea is to provide a general ordering of the analysis folders

To be able to recover analysis from anybody or just to accurately collaborate to a same project, we must put effort on writting good README files

README file
For each project (or sub-project) which contain the bin, data and doc structure the README file should contain a section for each analysis done.

This section should gives at least the following information:

Main objective of the analysis
Home-made scripts and bash scripts used (the scripts should be available in the bin folder)
Name and VERSION of external softwares/pipeline used (not included in the bin folder)
Which input files have been used (location in the data folder structure or which raw data)
Output/results file names
It should be also interesting to indicate in each section which parameters have ben used for the analysis or the comande line.

Feel free to also add specific recomandation for the analysis (e.g, specification on features due to the comptuing architecture) or for checking the output



