% Generated by roxygen2 (4.1.0): do not edit by hand
% Please edit documentation in R/pipeline.r
\name{getSupportedGeneSetsDB}
\alias{getSupportedGeneSetsDB}
\title{List supported gene set databases}
\usage{
getSupportedGeneSetsDB()
}
\description{
Return a data frame listing the supported gene set databases
}
\details{
This will return a data.frame listing the supported gene set databases
}
\author{
Lefebvre F
}

