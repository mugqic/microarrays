\name{projectInitialize}
\alias{projectInitialize}
\title{Initilize the 'project' database}
\usage{
  projectInitialize(path = db.path)
}
\arguments{
  \item{path}{path to where the skeleton of the db should
  be created}

  \item{create.full.structure}{Logical. Should a full
  project structure be created also: docs,bin, etc.}
}
\description{
  Initilize the 'project' database
}
\details{
  Project initialization consist in Building
}
\author{
  Lefebvre F
}

