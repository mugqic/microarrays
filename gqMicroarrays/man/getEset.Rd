% Generated by roxygen2 (4.1.0): do not edit by hand
% Please edit documentation in R/pipeline.r
\name{getEset}
\alias{getEset}
\title{Retrieve eset from DB}
\usage{
getEset(path = db.path)
}
\arguments{
\item{path}{path to the db}
}
\description{
Retrive annotated eset
}
\author{
Lefebvre F
}

