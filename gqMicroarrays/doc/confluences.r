

q()
R --vanilla


#setwd("/Users/flefebvr/Dropbox/projects/sandbox")
setwd("~/Dropbox/projects/sandbox")
library(roxygen2)


roxygenize('~/Dropbox/projects/github/Lef_AT_GenomeQuebec/gqUtils')
remove.packages('gqUtils')
system('R CMD build ~/Dropbox/projects/github/Lef_AT_GenomeQuebec/gqUtils')
system('R CMD install gqUtils_1.0.tar.gz')


roxygenize('~/Dropbox/projects/github/Lef_AT_GenomeQuebec/gqData')
remove.packages('gqData')
system('R CMD build ~/Dropbox/projects/github/Lef_AT_GenomeQuebec/gqData')
system('R CMD install gqData_1.0.tar.gz')

######################

library(roxygen2)
setwd("~/Dropbox/projects/sandbox")
remove.packages('gqMicroarrays')
roxygenize('~/Dropbox/projects/github/Lef_AT_GenomeQuebec/gqMicroarrays')
system('R CMD build ~/Dropbox/projects/github/Lef_AT_GenomeQuebec/gqMicroarrays')
system('R CMD install gqMicroarrays_1.0.tar.gz')






q()
R --vanilla
library(gqMicroarrays)
#setwd('/Users/flefebvr/Dropbox/projects/sandbox/testProject')
setwd('~/Dropbox/projects/sandbox/testProject')
system('rm -rf ~/Dropbox/projects/sandbox/testProject/*')


# only one variable needs to be defined in the glabal environment, db.path.
# it contains the path to the 'database' containing all of the projects' data
# e.g. data

db.path = 'data'
projectSkeleton()


checkSanity()


raw = loadRaw()


raw = qualityControl(raw)



eset = preProcess(raw)



# Example how to add additional figure
index = exploratoryAnalysis() 
#commitFigure() #...



# DGEA
fits = differentialAnalysis()
gc()
# PAthways


createReport()




############################
library(gqMicroarrays)
db.path = 'data'
projectSkeleton()
checkSanity()
## Pause to fill out worksheet.xls
express()
###########################


