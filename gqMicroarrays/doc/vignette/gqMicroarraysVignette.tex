%\VignetteIndexEntry{An Introduction to GenomicRanges}
%\VignetteDepends{Rsamtools}
%\VignetteKeywords{sequence, sequencing, alignments}
%\VignettePackage{GenomicRanges}
\documentclass[10pt]{article}

\usepackage{times}
\usepackage{hyperref}

\textwidth=6.5in
\textheight=8.5in
%\parskip=.3cm
\oddsidemargin=-.1in
\evensidemargin=-.1in
\headheight=-.3in

\newcommand{\Rfunction}[1]{{\texttt{#1}}}
\newcommand{\Robject}[1]{{\texttt{#1}}}
\newcommand{\Rpackage}[1]{{\textit{#1}}}
\newcommand{\Rmethod}[1]{{\texttt{#1}}}
\newcommand{\Rfunarg}[1]{{\texttt{#1}}}
\newcommand{\Rclass}[1]{{\textit{#1}}}
\newcommand{\Rcode}[1]{{\texttt{#1}}}

\newcommand{\software}[1]{\textsf{#1}}
\newcommand{\R}{\software{R}}
\newcommand{\Bioconductor}{\software{Bioconductor}}
\newcommand{\GenomicRanges}{\Rpackage{GenomicRanges}}


\title{Microarray Data Analysis and Reporting with \Rpackage{gqMicroarrays}}
\author{Francois Lefebvre}
\date{\today}

\usepackage{Sweave}
\begin{document}
\input{gqMicroarraysVignette-concordance}

\maketitle

\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
The \Rpackage{gqMicroarrays} package is a simple framework for the basic and 
advanced analysis of expression, miRNA and methylation microarrays. In essence,
it can be seen as a collection of wrappers, mostly around \software{Bioconductor},
and a some novel functions for quickly generating SOP-driven analysis results and reports.

\Rpackage{gqMicroarrays} depends on two other packages, \Rpackage{gqUtils} and 
\Rpackage{gqData}. \Rpackage{gqUtils} is a library of functions that
could eventually be shared across different pipelines, e.g. visualizations,
wrappers around embedded external tools or simple mathematical functions.
\Rpackage{gqData} houses larger datasets that could also eventually be used by
other packages, e.g. probe annotations, gene sets (pathways), genomic sequences,
etc.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pre-Requisites}

\subsection{Package Installation}
Here we assume the user has basic understanding how to install single packages and 
setup a local R library. See R and Bioconductor documentation for this. Note that
the package has only been tested on Linux and Mac OS.
\subsubsection{Resolving Dependencies}
Successfull installation \Rpackage{gqMicroarrays}, \Rpackage{gqUtils} 
and \Rpackage{gqData} requires resolving all dependencies first, i.e. installing
a buch of other R packages. Because those packages are not on CRAN or part of 
the Bioconductor project, dependencies will not resolve automatically.
To make you life easier, the following code will attempt the installation of a majority of them:
% deps = unique(c(strsplit(packageDescription('gqMicroarrays')$Depends,split='(, |,\n)')[[1]], strsplit(packageDescription('gqUtils')$Depends,split='(, |,\n)')[[1]], strsplit(packageDescription('gqData')$Depends,split='(, |,\n)')[[1]]))
\begin{Schunk}
\begin{Sinput}
> deps = readLines('http://dl.dropbox.com/u/2528754/gqMicroarrays/dependencies.txt')
> source("http://bioconductor.org/biocLite.R")
> install.packages(deps)
> biocLite(deps)
\end{Sinput}
\end{Schunk}
Following dependencies require manual installation or special attention:
\begin{itemize}
\item Package \Rpackage{Vennerable} can at downloaded at \url{https://r-forge.r-project.org/R/?group_id=474}.
\item Package \Rpackage{WriteXLS} requires a Perl module. See \url{http://cran.r-project.org/web/packages/WriteXLS/INSTALL}
\end{itemize}

\subsubsection{Building and Installing the \Rpackage{gq} Packages}
The packages are hosted in a private github repos for now. It will be moved to the internal 
svn when I figure out a way to access it from outside... . Installation can be done this way for now:
\begin{Schunk}
\begin{Sinput}
> download.file('http://dl.dropbox.com/u/2528754/gqMicroarrays/packages.zip','packages.zip')
> unzip('packages.zip')
> system('chmod +x gqUtils/configure') # necessary for some reason
> system('R CMD build gqUtils ; R CMD build gqData ; R CMD build gqMicroarrays')
> install.packages(c('gqUtils','gqData','gqMicroarrays'), repos=NULL)
\end{Sinput}
\end{Schunk}




\subsection{Gathering Information about the Project}
\begin{itemize}
\item {\bf The array platform and platform version}. For instance, {\em Agilent-039494
 SurePrint G3 Human GE v2 8x60K Microarray} is an Agilent human expression array, 
 version number 039494. The core will generally email the version 
 information to the client (for him to retrieve probe annotations), but not to the analyst.
 The version information is also not always available in Nanuq, but it can either be guessed
  by comparing the probe names in the raw data files to GEO records, 
  or simpy by asking the core and waiting for a reply ;)
\item {\bf The experimental design}. The sample sheet alone will generally not convey
  enough information about the design of the study.
  In order to send meaningful statistical
  results to the client, it is crucial for the analyst to understand the design of the study:
  What are the relevant experimental factors/variables? What is the sampling 
  unit of this study (e.g. patient, animal, cell sort)? What is the sampling structure? 
  (paired, unpaired, double-paired etc.)
  Are there technical replicates? batches? What effects should be tested
  (e.g. interactions)? Sometimes, the experimental designs will be appropriately
  described in the documents attached to the the project in Nanuq, sometimes
  the client needs to be contacted directly.
\item {\bf Aims of the study, prior expectations}. Knowing a bit about the biology
  of the project is always helpful in terms of delivering targeted figures, 
  but also for troubleshooting. Microarrays data analysis is more mature and much simpler
  if compared to sequencing. As a consequence, the analyst has more time to spend on the interpretation
  and visualization of results, rather than on writing and running scripts
  that produce raw data.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preparing the input: parameters and sample annotations}

\subsection{Initializing the project folder}
One the package is installed, start an R session from, or set the working directory
of to a project folder of your choice. The next useful thing to do is to define
the global variable \Robject{db.path}. This variable defines the {\bf relative} path
to a folder that will hold the "database", i.e. annotations, resulting R objects, etc.
Here, we will call this filter 'data'. Project Genest003 will serve as a use case.
\begin{Schunk}
\begin{Sinput}
> library(gqMicroarrays)
\end{Sinput}
\begin{Soutput}
Welcome to Rsge
    Version: 0.6.3 
\end{Soutput}
\begin{Sinput}
> db.path = 'data'
\end{Sinput}
\end{Schunk}

Next, running the function \Rfunction{projectSkeleton()}
will initialize the content of this folder by creating a pre-determined structure.
\begin{Schunk}
\begin{Sinput}
> projectSkeleton()
\end{Sinput}
\end{Schunk}
Most importantly \Rfunction{projectSkeleton()} will create \Robject{db.path} with {\em worksheet.xls} in it.
The latter file plays a central role in the pipeline: its first sheet lists the 
{\em parameters}; its second sheet holds the {\em sample annotations} (this will 
be refered to as the {\em pdata}). Sheets 3 and up can
be used for notes, logs,  store other data tables, etc.

\subsection{Filling out the sample annotation (pdata)}
Unfortunately, human intervention is necessary at this point. the reason is
very simple: the sample sheet does not list experimental factors relevant for
statistical analysis, the information is contains is not necessarly well formatted, etc.
More generally, this reflects the fact that the statistical analysis of a designed
experiment is not easily automated.

The second sheet of {\em worksheet.xls}  (we will call this table the pdata)
should already be filled with template content. Notice the special rows and columns of pdata:
\begin{itemize}
\item The first row, called HIDDEN, is used to hide columns.  Every column for
which this cells in this row are non-empty will be ignored when reading the pdata.
Hidden columns will therefore not appear in the report to the client. This is useful to
hide technical details or potentially embarrassing notes about samples.
\item The second row, NUMERIC works similarly to the first. Columns flagged here
will be read as numerical variables instead of character. This can be useful if
some experimental variables should be treated as continuous in the linear model, e.g. antibody titers, time point, etc.
\item The first column serves the same purpose as the HIDDEN row. Arrays flagged 
in this way will be excluded from the analysis. This can be useful of some of them are not available yet,
or are failed hybs we do not want to show the client.
\end{itemize}

A good idea at this point would be to append the sample sheet retrieved from Nanuq 
as the last columns of pdata. The next thing to do is to fill out the following 
columns:
\begin{itemize}
\item {\bf ArrayID} (mandatory). Define a unique identifier for the array.
Typically this can mean simply copying the column 'Sample' the Nanuq sample sheets.
\item {\bf SampleID} (mandatory). Define an identifier for samples. Because of
technical replicates, the relationship between samples and arrays is not always
1:1. In most cases however, this will be identical to ArrayID.
\item {\bf FilePath} (mandatory). Enter the path to each array. This might get
automatized in some way later.
\item {\bf outlierFlag} (mandatory). This column is used to flag arrays the analysts
wants to remove from the final analysis. In the pipeline, the removal process takes place
after generating the QC diagnostic plots. The flag itself should be a succint \underline{description
of the reason for removal}. This description will appear in the final report.
\item {\bf batch} (mandatory). This column is mandatory but needs to be filled out
only if one intends to perform batch correction with ComBat (rarely used).
\item {\bf Others columns} (depends on the design). Create and fill out any other column
that define experimental variables. Typically, these will be the PatientID and treatment variables.
Make sure the levels of those factors are synthaxially valid. Level names should not start with a 
number and not contain math operators. Finally, be creative in terms of names for variables and 
their levels; do not shy away from using names the client will understand.
Avoid unintelligible names like "Factor1" or "Group1". Show the client you are not just
turning the crank on a pipeline.
\end{itemize}
Any other columns can be added here. For instance, I like to keep track of what
the boxplots, dendrograms, etc. look like. Or create a note column to track
comments from the lab.


\subsection{Filling out the parameters list}

The first sheet in {\em worksheet.xls} lists parameters. A rough description 
of those parameters is offered in the Description columns. Here we will elaborate on
the non-trivial ones:
\begin{itemize}
\item {\bf platform.alias} (mandatory). This is a character string which refers
to the array platform of the experiment. The function \Rfunction{getSupportedPlatforms()}
returns a data.frame listing all supported platform and the corresponding alias.
More platforms will be made available as we encounter them.
\begin{Schunk}
\begin{Sinput}
> getSupportedPlatforms()[,1,drop=FALSE]
\end{Sinput}
\begin{Soutput}
                                                                                          Platform
Agilent028005                                Agilent-028005 SurePrint G3 Mouse GE 8x60K Microarray
Agilent039494                             Agilent-039494 SurePrint G3 Human GE v2 8x60K Microarray
IlluminaHumanMethylation450                             Infinium HumanMethylation450 BeadChip Kit 
Affymetrix_miRNA_2_0                                       Affymetrix Multispecies miRNA-2_0 Array
Agilent035430               Agilent-035430 mouse miRNA array (miRBase release 18 miRNA ID version)
IlluminaExpressionHT12V4                                         HumanHT-12 v4 Expression BeadChip
\end{Soutput}
\end{Schunk}
\item {\bf gene.sets.db.alias} (optional). This is similar to platform.alias, but
for gene sets testing. Leave blank to omit gene set analysis.
\begin{Schunk}
\begin{Sinput}
> getSupportedGeneSetsDB()[,1,drop=FALSE]
\end{Sinput}
\begin{Soutput}
                                    Database
ipa_human Ingenuity Canonical Pathways Human
ipa_mouse Ingenuity Canonical Pathways Mouse
ipa_rat     Ingenuity Canonical Pathways Rat
\end{Soutput}
\end{Schunk}
\item {\bf batch.adjust.covariates} (optional) Name of the (categorical only)
variables to be included when running ppComBat. The names refer to column names of pdata,
separated by a comma. E.g. "Time,Mutation". Leave blank for no batch correction.
\item {\bf avg.techreps.by} (optional) Name of the (categorical only)
variables to average by, across arrays. Typically, we will want to average arrays
who originate from the same stock sample, so set this to "SampleID". Leave blank for no averaging.
\item {\bf sample.description.string} (optional) Columns in pdata to paste
together as a sample descriptor in figures. This will typically be 
SampleID,Var1,Var2, etc.
\item {\bf dgea.variables} (mandatory) Columns in the pdata that represent the 
variables in the linear model. The default behavior of the pipeline is to report
contrasts of an AVOVA 1 or 2-way design. The \underline{last two variables} specified by
dgea.variables will vary, the remaining will be fixed. For more complex designs, e.g. paired,
multiple variables, etc., you should still enter the variables here, but the pipeline will have to be
run explicitely, with the design and contrast matrices defined within R (see section "Dealing with complex designs").
\item {\bf dgea.variables.base.levels} (mandatory). Baseline levels for each of the above specified dgea.variables. This 
will define hat contrasts are reported by default in the express mode. Eventually, the express mode might
report all possible pairwise comparisons if the model includes only one variable. 
\end{itemize}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Express Run}

For simple designs, which might account for a large proportion of projects, the \Rfunction{express()}
function will generate a report in one shot. In the process, the function \Rfunction{checkSanity()} is called. This function
performs multiple checks to ensure parameters and annotations are valid and consistent with one another. 
It should return meaningful message that help fix problems.
\begin{Schunk}
\begin{Sinput}
> express()
\end{Sinput}
\end{Schunk}

If all goes well, a Report folder should have been created!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Explicit step by step run}
Typically, this will be useful for complicated designs or when one wants to
append custom Qc and exploratory functions to the report.

Coming soon....




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Adding custom QC or exploratory Figures to the Report}
The mechanism to do this right now is too complicated. I will simplify this soon.

Coming soon....

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dealing with complex designs}

Coming soon....

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Advanced Analysis}
The package does not only contain functions for the SOP-driven reporting of 
differential expression analysis....

Coming soon....

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Advanced: Adding support for a new chip version in \Rpackage{gData} }

Coming soon....

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Advanced: Description of the Package Architecture}
%The architecture of the package is quite simple. In order to handle multipe
%platform-specific pipelines in a single package, general functions such as
%\Rfunction{qualityControl()} dispatch work to more specific functions such as
%\Rfunction{qualityControl.agilent()}. Which specific function is called depends
%on the platform.alias specified by the user in the parameter file, which in turn
%is mapped to a specific pipeline or pipeline.type as predefined info.xls
%table.
%Retriving probe annotations is very often an annoying task 
%As the pipeline proceeds through the analysis steps, R objects and files are 
%written to the 'database' folder, db.path. The createReport() function then 
%operates on this folder to generate an HMTML report.
% Reporting is decoupled
Coming soon....

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Advanced: Implementing a new Pipeline}
Coming soon....




\end{document}
