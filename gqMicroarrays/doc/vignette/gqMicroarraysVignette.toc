\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Pre-Requisites}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Package Installation}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Resolving Dependencies}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Building and Installing the {\textit {gq}} Packages}{2}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Gathering Information about the Project}{2}{subsection.2.2}
\contentsline {section}{\numberline {3}Preparing the input: parameters and sample annotations}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Initializing the project folder}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Filling out the sample annotation (pdata)}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Filling out the parameters list}{4}{subsection.3.3}
\contentsline {section}{\numberline {4}Express Run}{5}{section.4}
\contentsline {section}{\numberline {5}Explicit step by step run}{5}{section.5}
\contentsline {section}{\numberline {6}Adding custom QC or exploratory Figures to the Report}{5}{section.6}
\contentsline {section}{\numberline {7}Dealing with complex designs}{5}{section.7}
\contentsline {section}{\numberline {8}Advanced Analysis}{6}{section.8}
\contentsline {section}{\numberline {9}Advanced: Adding support for a new chip version in {\textit {gData}} }{6}{section.9}
\contentsline {section}{\numberline {10}Advanced: Description of the Package Architecture}{6}{section.10}
\contentsline {section}{\numberline {11}Advanced: Implementing a new Pipeline}{6}{section.11}
