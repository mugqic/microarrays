#' This function removes outliers flagged in the array annotation. The authoritative flag is the one 
#' in worksheet.xls, not the one in whatever annotation of the raw object.
#' @title Remove outliers
#' @param path path to where the skeleton of the db should be created
#' @author Lefebvre F 
#' @note For now the assumption if that raw objects can be subsetted. This should be valid for most pipelines
#' @export
removeOutliers <- function(obj,path=db.path)
{
# obj = raw;path=db.path

	# Hard-coded mapping between pipelines and types
	#types = c('AgilentGeneExpression'='agilent')

	# Load the database content
	pdata    = getPdata(path)
	params   = getParams(path)
	outliers = rownames(pdata)[ pdata$outlierFlag != '']
	cat(paste('\nWill remove the following arrays:\n',paste(outliers,collapse=', '),'\n\n',sep=''))
	
	if( is(obj,'ExpressionSet'))
	{
		obj = obj[,!sampleNames(obj) %in% outliers]
	} else
	{
		obj  = obj[,!colnames(obj) %in% outliers]
	}
	

	index = pdata[outliers, unique(c('ArrayID','SampleID','outlierFlag',params[['sample.description.string']])) ]
	colnames(index)[3] = 'Reason for Removal'	

	

	return( obj )
}
