

#' This function implements SOP exploratory analysis. Note that additional figures can be genrated outside of the SOP and still be reported
#' 
#' @title SOP Exploratory Analysis
#' @param path db.path
#' @author Lefebvre F
#' @note This is abstracted not by pipeline but by pipeline.type. We assume for instance all exprs arrays are analyzed the same way
#' @export 
exploratoryAnalysis <- function(obj=getEset(), path=db.path)
{
#obj=getEset(); path=db.path
	expl.path = file.path(path,'exploratory')
	unlink(expl.path,recursive=TRUE,force=TRUE)# cleanup
	dir.create(expl.path,showWarnings=FALSE, recursive=TRUE)
	
	# Hard-coded mapping between pipelines and types
	#types = c('expression'='expression')

	# Load the database content
	params   = getParams(path)
	pipeline.type = getSupportedPlatforms()[params[['platform.alias']],'pipeline.type']

	# Prepare some variables...
	col.labels = params[['sample.description.string']]
	#if(!is.null(col.labels))pData(obj)[['.col.labels']] = do.call(paste,pData(obj)[,col.labels])
	cols.ann   = params[['dgea.variables']]

	
	# Call the necessary method # TOODO: more advanced R with methods would be nice, alas
	index = eval(call(name=paste('exploratoryAnalysis',pipeline.type,sep='.')
		,e          = obj
		,path       = expl.path
		,col.labels = col.labels
		,cols.ann   = cols.ann
		))

	index = data.frame('Index'=c(1:nrow(index)),index)	
	colnames(index)= c('Index','Description','File')
	index.html = index
	index.html[['File']] = hwrite('pdf',link=index.html[['File']])
	index =  list('index'=index,'index.html'=index.html)


	save(index,file=file.path(expl.path,'index.RData'))
	return(index)
}
# TODO: this whole system of col and ann and labels is too complicated



#' This function implements standard SOP exploratory analysis. Note that additional figures can be genrated outside of the SOP and still be reported
#' 
#' @title SOP generic exploratory analysis plots
#' @param e An ExpressionSet object
#' @param path path to where the exploratory figures will be written
#' @param col.labels character vector label listing which column in pData(e) should be used as label
#' @param row.labels character vector label listing which column in fData(e) should be used as label
#' @param cols.ann character vector label listing which column in pData(e) should be used as additional annotations, e.g. color, shape, color bars at the top of heatmap
#' @author Lefebvre F
#' @export
exploratoryAnalysis.MeDIP <- function(e,path='.',col.labels='SampleID',row.labels='SYMBOL',cols.ann=NULL)
{
	pData(e)[['.col.labels']]  = do.call(paste,pData(e)[,col.labels,drop=FALSE]) # more convienient to do this right away
	if(!is.null(cols.ann))
	{
		df = pData(e)[,cols.ann,drop=FALSE]
		df = df[,rev(colnames(df)),drop=FALSE] # because we instructed to put the most important one last
		df = df[,1:min(2,ncol(df)),drop=FALSE]
		
	}else{df=NULL}
		# df is a data frame of annotations to supply to the uA-style plots.. they require a df of lenght 2 max
	
	l = list(
		uA.cordist.hclust(e, path, alt.colnames = pData(e)[['.col.labels']]) # hclust
		,uA.mds(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# MDS
		,uA.pca(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# PCA
		)

	# Heatmap of top sd genes. # 
	fn = file.path(path,'top_sd_heatmap.pdf')
	lim = ceiling(max(abs(exprs(e))))
	sat = 3
	if(lim>sat){breaks = c(-lim,seq(-sat,sat,by=0.2),lim) }else{breaks = seq(-lim,lim,by=0.2)}
	; col=colorpanel(length(breaks)-1,'blue','black','red')
	pheatmap.eset(e,file=fn,scale='none',row.labels=row.labels,col.labels=col.labels,cols.ann=cols.ann, breaks=breaks, col=col)
	l = c(l,list(data.frame('Description'='Heat map of the log ratios for the most across samples varying probes (top SD)','path'=fn)))

	index = do.call(rbind,l)

}





#' This function implements standard SOP exploratory analysis. Note that additional figures can be genrated outside of the SOP and still be reported
#' 
#' @title SOP generic exploratory analysis plots
#' @param e An ExpressionSet object
#' @param path path to where the exploratory figures will be written
#' @param col.labels character vector label listing which column in pData(e) should be used as label
#' @param row.labels character vector label listing which column in fData(e) should be used as label
#' @param cols.ann character vector label listing which column in pData(e) should be used as additional annotations, e.g. color, shape, color bars at the top of heatmap
#' @author Lefebvre F
#' @export
exploratoryAnalysis.expression <- function(e,path='.',col.labels='SampleID',row.labels='SYMBOL',cols.ann=NULL)
{
	pData(e)[['.col.labels']]  = do.call(paste,pData(e)[,col.labels,drop=FALSE]) # more convienient to do this right away
	if(!is.null(cols.ann))
	{
		df = pData(e)[,cols.ann,drop=FALSE]
		df = df[,rev(colnames(df)),drop=FALSE] # because we instructed to put the most important one last
		df = df[,1:min(2,ncol(df)),drop=FALSE]
		
	}else{df=NULL}
		# df is a data frame of annotations to supply to the uA-style plots.. they require a df of lenght 2 max
	
	l = list(
		uA.cordist.hclust(e, path, alt.colnames = pData(e)[['.col.labels']]) # hclust
		,uA.mds(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# MDS
		,uA.pca(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# PCA
		)

	# Heatmap of top sd genes. # TODO: might be more elegant to return index like all others but eh.
	fn = file.path(path,'top_sd_heatmap.pdf')
	pheatmap.eset(e,file=fn,scale='row',row.labels=row.labels,col.labels=col.labels,cols.ann=cols.ann)
	l = c(l,list(data.frame('Description'='Heat map of most varying probes (top SD)','path'=fn)))


	# PVCA
	fn = file.path(path,'pca_anova.pdf')
	pca.anova.eset(e,fn=fn,10)
	l = c(l,list(data.frame('Description'='ANOVA R2 of covariates on the first Principal Components','path'=fn)))



	index = do.call(rbind,l)

}
# TODO: shouldn't this function be pipeline agnostic?. params arg breaks this
# Also, possible: bug; shape is limited to 8, so ggplot will complain if too many levels.
# I don't like these functions anyway... they are complex becaue they hae to be applied to qc and expl,
# but then , why not coeece to eset at qc and have the uA functions operate on esets...


#' This function implements standard SOP exploratory for methylation... right now, same as expression!
#' 
#' @title SOP generic exploratory analysis plots
#' @param e An ExpressionSet object
#' @param path path to where the exploratory figures will be written
#' @param col.labels character vector label listing which column in pData(e) should be used as label
#' @param row.labels character vector label listing which column in fData(e) should be used as label
#' @param cols.ann character vector label listing which column in pData(e) should be used as additional annotations, e.g. color, shape, color bars at the top of heatmap
#' @author Lefebvre F
#' @export
exploratoryAnalysis.methylation <- function(e,path='.',col.labels='SampleID',row.labels='SYMBOL',cols.ann=NULL)
{
	exploratoryAnalysis.expression(e=e,path=path,col.labels=col.labels,row.labels=row.labels,cols.ann=cols.ann)
}
# TODO: 450k tailored EDA!!!!!




#' This function implements standard SOP exploratory analysis. Note that additional figures can be genrated outside of the SOP and still be reported
#' 
#' @title SOP generic exploratory analysis plots -- microRNA
#' @param e An ExpressionSet object
#' @param path path to where the exploratory figures will be written
#' @param col.labels character vector label listing which column in pData(e) should be used as label
#' @param row.labels character vector label listing which column in fData(e) should be used as label
#' @param cols.ann character vector label listing which column in pData(e) should be used as additional annotations, e.g. color, shape, color bars at the top of heatmap
#' @author Lefebvre F
#' @export
exploratoryAnalysis.microRNA <- function(e,path='.',col.labels='SampleID',row.labels='SYMBOL',cols.ann=NULL)
{
	pData(e)[['.col.labels']]  = do.call(paste,pData(e)[,col.labels,drop=FALSE]) # more convienient to do this right away
	if(!is.null(cols.ann))
	{
		df = pData(e)[,cols.ann,drop=FALSE]
		df = df[,rev(colnames(df)),drop=FALSE] # because we instructed to put the most important one last
		df = df[,1:min(2,ncol(df)),drop=FALSE]
		
	}else{df=NULL}
		# df is a data frame of annotations to supply to the uA-style plots.. they require a df of lenght 2 max
	
	l = list(
		uA.cordist.hclust(e, path, alt.colnames = pData(e)[['.col.labels']]) # hclust
		,uA.mds(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# MDS
		,uA.pca(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# PCA
		)

	# Heatmap of top sd genes. # TODO: might be more elegant to return index like all others but eh.
	fn = file.path(path,'top_sd_heatmap.pdf')
	pheatmap.eset(e,file=fn,scale='row',row.labels=row.labels,col.labels=col.labels,cols.ann=cols.ann)
	l = c(l,list(data.frame('Description'='Heat map of most varying probes (top SD)','path'=fn)))

	index = do.call(rbind,l)

}














