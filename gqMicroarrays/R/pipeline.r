###
### General Pipeline functions
###
.onAttach <- function(libname, pkgname)
 {
 	options(stringsAsFactors=FALSE)
	theme_set(theme_bw())
 	packageStartupMessage('\n\ngqMicroarrays loaded.\n\n')
 }

#' Initialize a new project folder by creating directories and copying some necessary files.
#' 
#' This function creates a data folder to the working directory and populates it with template files. Most
#' importantly, it creates the params.txt file and pdata.txt file which need editing.
#'
#' @title Create a project folder
#' @param path path to where the skeleton of the db should be created
#' @param create.full.structure Logical. Should a full project structure be created also: docs,bin, etc. 
#' @author Lefebvre F
#' @export
projectSkeleton <- function(path=db.path, create.full.structure=TRUE)
{
	paths = path
	if(create.full.structure)
	{
		paths = c(paths,'bin','doc','deliverables') 
		file.copy(system.file(file.path('templates','README.txt')
			,package = "gqMicroarrays"),'.',overwrite=FALSE)# copy the README.txt	 
	}	
	sapply( paths[!file.exists(paths)] , dir.create )
	
	# Copy the worksheet
	file.copy(system.file(file.path("templates",'worksheet.xls'),package = "gqMicroarrays"), path, overwrite=FALSE)

	cat('\n\n\n*****\nTemplates copied. Fill out the params and pdata sheets in worksheet.xls\n\n*****\n\n\n')		
}

#' Return a data.frame listing the supported microarray platforms
#' 
#' This function will return a data.frame listing the supported microarray platforms
#'
#' @title List supported platforms
#' @author Lefebvre F
#' @export
getSupportedPlatforms <- function()
{
	platforms = read.xls(system.file(file.path("info",'supported.xls'),package = "gqMicroarrays"),sheet=1, stringsAsFactors=FALSE)
	rownames(platforms) = platforms[['alias']]
	return(platforms)
}

#' Return a data frame listing the supported gene set databases
#' 
#' This will return a data.frame listing the supported gene set databases
#'
#' @title List supported gene set databases
#' @author Lefebvre F
#' @export
getSupportedGeneSetsDB <- function()
{
	gs = read.xls(system.file(file.path("info",'supported.xls'),package = "gqMicroarrays"),sheet=2, stringsAsFactors=FALSE)
	rownames(gs) = gs[['alias']]
	return(gs)
}

#' Get the samples annotation from the worksheet
#' 
#' This function will read the second sheet in worksheet.xls, which holds annotation for arrays. This sheet has a special format,
#' with the first row indicating to hide or not a given column. The second row indicates the type of column (numerical or not ).
#' The first column is a hide indicator on arrays.... Since this file is edited by the user, checks should be default and detailed...
#'
#' @title Get the samples annotation
#' @param path path db.path where worksheet.xls is found
#' @author Lefebvre F
#' @export
getPdata <- function(path = db.path)
{
	# First read the indicator columns and rows
	headers = read.xls(file.path(path,'worksheet.xls')
	,sheet=2,skip=0,check.names=FALSE,nrows=2,stringsAsFactors=FALSE,header=FALSE,comment.char='',
	colClasses='character')
	visible.cols = gsub(' *','', as.character(headers[1,]  ) )[-1] == '' # this reads the first line and removes spaces 
	numeric.cols = gsub(' *','', as.character(headers[2,]  ) )[-1] != '' # this reads the first line and removes spaces

	# Read-in the actal data
	pdata = read.xls(file.path(path,'worksheet.xls'),sheet=2,skip=2,check.names=FALSE
		,stringsAsFactors=FALSE,header=TRUE,comment.char='',colClasses='character') #  ,colClasses=colClasses
	
	# Apply masks
	pdata = pdata[pdata[,1]=='',] # mask on rows
	pdata[,1] = NULL 
	for(i in which(numeric.cols)){pdata[,i] = as.numeric(pdata[,i])} # numeric maskl
	pdata = pdata[,visible.cols] # remove hidden cols

	
	## Basic sanity checks...
	# Verify  the presence of mandatory columns
	mandatory.columns = c('ArrayID','SampleID','FilePath','outlierFlag','batch')		
	if( any(! mandatory.columns  %in% colnames(pdata)) )
	{
		stop(paste("Malformed pdata. Columns ",paste(mandatory.columns,collapse=', '),' should be present.',sep=''))
	}
	# Primary key
	if(anyDuplicated( pdata[['ArrayID']] ) | any(pdata[['ArrayID']]==''))
	{
		stop(paste('ArrayID in the sample annotation (second sheet in worksheet.xls) has to be a unique identifier',sep=''))	
	}
	rownames(pdata) = pdata[['ArrayID']]
	# Issue warnings for SampleID and FilePath
	if(any(pdata[['SampleID']]=='')){stop('There is an undefined SampleID in the array annotation')}
	if(any(pdata[['FilePath']]=='')){warning('There is an undefined FilePath in the array annotation')}
	return(pdata)
}

#' Access the parameters file
#' 
#' This function fetches the param file...
#'
#' @title Read the parameter values file
#' @param path path to where the skeleton of the db should be created
#' @param create.full.structure Logical. Should a full project structure be created also: docs,bin, etc. 
#' @author Lefebvre F
#' @export
getParams <- function(path=db.path,split.char=' *, *')
{
	# Append some more parameters to it, like data, R version, attached packages
	x = read.xls(file.path(path,'worksheet.xls'),colClasses='character',stringsAsFactors=FALSE,
			blank.lines.skip=TRUE,na.strings='')
	params = strsplit(x[['Value']],split=split.char)
	names(params) = x[['Parameter']]
	params = mapply(function(p,t){ as(p,t) },params,x[['Type']],SIMPLIFY=FALSE) # Type the parameters

	# Turn NAs into NULL
	params = lapply(params,function(x){  if(any(is.na(x))){return(NULL)}else{x}        })
	#return(params)

	# Append additional parameters.. some for reproducubility that can be written somehere elses
	params[['date']] = date()
	params[['session.info']] = sessionInfo()
	params[['options']] = options()

	# the params sheet structure is not edited by the user, so no checks are really necessary here.
	if(is.null(params[['author']])){ params[['author']] = 'Genome Quebec, Bioinformatics Services'}
	if(is.null(params[['title']])){ params[['title']] = 'Microarray Analysis Report'}
	if(is.null(params[['contact']])){ params[['contact']] = 'bioinformatics.genome@mail.mcgill.ca'}
	if(is.null(params[['report.dir']])){ params[['report.dir']] = paste('Report')}
	if(is.null(params[['mc.cores']])){ params[['mc.cores']] = 2}
	if(is.null(params[['sample.description.string']])){ params[['sample.description.string']] = 'SampleID'}

	if(is.null(params[['count.fc']])){ params[['count.fc']] = 1.5}
	if(is.null(params[['count.p']])){ params[['count.p']] = 0.01}
	if(is.null(params[['count.fdr']])){ params[['count.fdr']] = 0.05}
	if(is.null(params[['html.top.tables.n']])){ params[['html.top.tables.n']] = 500}
	if(is.null(params[['top.heatmaps.n']])){ params[['top.heatmaps.n']] = 50}	

	return(params)
}

#' Get a probe annotation from the gqData package
#' 
#' This function will import a probe annotation specified by an alias into the data folder.
#' @param alias platform alias from gqData to import
#' @param import whether or not the loaded fdata should be saved to path
#' @param path Where the loaded object should be saved if import is TRUE
#' @param ret.r whether or not the fdata should be returned by the function
#' @title get the probe annotations
#' @author Lefebvre F
#' @export
getFdata <- function(alias=getParams()[['platform.alias']], import=FALSE, path='', ret.r=TRUE)
{
	require(gqData)
	supported = getSupportedPlatforms()

	if(! alias %in% supported$alias )
	{
		stop(paste(alias,'is not an alias for a supported platform, which are:',paste(supported$alias,collapse=', '),'. See getSupportedPlatforms() for full description.'))
	}
	data(list=alias,package='gqData', envir=environment()) # this loads an object names fdata		
	if(import)
	{
		save(fdata,file=file.path(path,'fdata.RData'))
	}
	if(ret.r)
	{
		return(fdata)
	}
}

#' Get the gene sets DB from db.path
#' 
#' This function will return the gene sets saved in db.path
#' @param alias the gene set DB alias, see getSupportedGeneSetsDB()
#' @param import whether or not the loaded gene sets should be saved to path
#' @param path Path to where object is saved .necessary only if import=TRUE
#' @param ret.r whether or not the gene sets should be returned by the function
#' @param as.list whether the gene sets should be returned as list (default) or the full data.frame 
#' @param paste.members Column Members in the loaded object is really a list. Should it be collapsed with paste(sep=', ') 
#' @title Get the gene sets database
#' @author Lefebvre F
#' @export
getGeneSets <- function(alias=getParams()[['gene.sets.db.alias']], import=FALSE, path='', ret.r=TRUE,as.list=TRUE, paste.members=TRUE)
{
	require(gqData)
	supported = getSupportedGeneSetsDB()

	if(is.null(alias)){return(NULL)}

	if(! alias %in% supported$alias )
	{
		stop(paste(alias,'is not an alias for a supported gene set db, which are:',paste(supported$alias,collapse=', '),'. See getSupportedGeneSetsDB() for full description.'))
	}
	data(list=alias,package='gqData', envir=environment()) # this loads an object named gene.sets	
	
	if(import)
	{
		save(gene.sets,file=file.path(path,'geneSets.RData'))
	}
	if(ret.r)
	{
		gs = gene.sets
		if(as.list) # Convenient to have as list for computing, table convienent for reporting...
		{
			gs = gs[['Members']]
			names(gs) = rownames(gene.sets) 
			return(gs)
		}
		if(paste.members)
		{
			gs[['Members']] = sapply(gs[['Members']] ,function(x)  paste(x,collapse=','))
		}	
		return(gs)
	}
}

#' Make checks
#' 
#' Make checks
#'
#' @title Make checks
#' @param path path to where the skeleton of the db should be created
#' @author Lefebvre F
#' @export
checkSanity <- function(path = db.path)
{
	cat('Running Checks on parameters, sample annotation and chip files....\n\n')

	# The two thgins to check are params and pdata... the rest should be fine!, e.g. gene sets should be consistent with 
	# fdata, u.s.w

	cat('Attempting to read params in sheet 1 of worksheet.xls...')
	params = getParams(path)
	cat('ok.\n')
	
	cat('Attempting to read sample annotations in sheet 2 of worksheet.xls...')
	pdata  = getPdata(path)	 # this implicitely does checks on primary key and mandatory columns...
	cat('ok.\n')
	
	cat('Attempting to retrieve the feature annotations specified by platform.alias in the parameters...')
	fdata = getFdata(alias = params[['platform.alias']])
	cat('ok.\n')

	cat('Checking whether chip files exist...')
	if(any(!file.exists( pdata[['FilePath']]  ))){stop('Error: Cannot locate all files specified in column FilePath of sample annotations')}
	cat('ok.\n') 
	
	cat('Peeking at the first chip to make sure correct platform....')
	peek = loadRaw(path,peek=TRUE)
	cat('ok.\n')

	cat('Inspecting parameter related to batch adjustment...')
	if( !is.null( params[['batch.adjust.covariates']] ) )
	{
		if(any(! params[['batch.adjust.covariates']] %in% colnames(pdata)       ))
		{
			stop('Error: Some covariates specified in parameter batch.adjust.covariates do not appear as variables in the sample annotation. Check this.')
		}
		x = unlist(pdata[,c('batch',params[['batch.adjust.covariates']])])
		if(any( x =='' | is.na(x) )){stop('Error: batch or one of the covariates specified in parameter batch.adjust.covariates have unspecifed values or levels')}
	}
	cat('ok.\n')


	cat('Inspecting parameter related to the averaging of technical replicates...')
	if( !is.null( params[['avg.techreps.by']] ) )
	{
		if(any(! params[['avg.techreps.by']] %in% colnames(pdata)       ))
		{
			stop('Error: One of the variables specifying how to avg. tech reps  does not appear as variables in the sample annotation. Check this.')
		}
		x = unlist(pdata[,c(params[['avg.techreps.by']])])
		if(any( x =='' | is.na(x) )){stop('Error: One of the variables specifying how to avg. tech reps has missing values')}
	}
	cat('ok.\n')

	cat('Inspecting variables to use for sample labeling...')
	if(any(! params[['sample.description.string']] %in% colnames(pdata)       ))
	{
		stop('Error: One of the variables in sample.description.string specifying which variables should label samples do not appear as variables in the sample annotation. Check this.')
	}
	x = unlist(pdata[,c(params[['sample.description.string']])])
	if(any( x =='' | is.na(x) )){stop('Error: One of the variables in sample.description.string has missing values. Check this')}
	cat('ok.\n')





	cat('Inspecting variables used for default differential analysis...')
	if(!is.null ( params[['dgea.variables']] ))
	{
		if(any(! params[['dgea.variables']] %in% colnames(pdata)       ))
		{
			stop('Error: One of the variables specified for the default differential analysis does not appear as variables in the sample annotation. Check this.')
		}
		x = unlist(pdata[,c(params[['dgea.variables']])])
		if(any( x =='' | is.na(x) )){stop('Error: One of the variables specifying the default differential analysis has missing values')}
	
		for(v in params[['dgea.variables']])
		{
			#print(pdata[[v]])
			if(is.character(pdata[[v]]) & any( pdata[[v]] != make.names(pdata[[v]])))
			{	
				stop('Error: One of the variables specifying the default differential analysis has R synthax invalid values. Correct this')
			}
		}

	}else{stop('Parameter dgea.variables should specify at least one variable for diff. analysis')}
	cat('ok.\n')


	# Issue warning about numerical covariates
	cat('Inspecting baseline levels of variable used for diff. analysis...')
	if(length( params[['dgea.variables.base.levels']] )  !=  length( params[['dgea.variables']] ))
	{
		stop('Error: Number of variables specified by dgea.variables does not match the number in dgea.variables.base.levels')
	}
	for(i in 1:length(params[['dgea.variables']]))
	{
		lev = 	params[['dgea.variables.base.levels']][i]
		v   =   pdata[, params[['dgea.variables']][i] ]
		if( is(v,'character')  & ! lev %in% v ){stop('Error: One of the base level in dgea.variables.base.levels does not match its corresponding categorical variable')}
	}
	cat('ok.\n')





	# TODO: implement checks? on other parameters? Not really necessary
	#dgea.variables
	#count.fc
	#count.p
	#count.fdr
	#html.top.tables.n
	#top.heatmaps.n	
	#cat('Attempting to...')
	#cat('ok.\n')

	if(!is.null(params[['gene.sets.db.alias']])){
		cat('Attempting to retrieve gene sets as specified gene.sets.db.alias in the parameters...')
		gene.sets = getGeneSets(alias=params[['gene.sets.db.alias']])
		cat('ok.\n')
	
		cat('Inspecting mapping between gene sets DB and probe annotation: ')
		gs.ids = unique(unlist(gene.sets))
		fd.ids = unique(fdata[['ENTREZID']])
		venn = list(setdiff(gs.ids ,fd.ids),intersect(gs.ids ,fd.ids),setdiff(fd.ids,gs.ids))
        	cat(paste('The gene universe of the gene set DB has',length(gs.ids),'unique entrez ids. The gene universe of the probe annotation has'
			,length(fd.ids),'unique entrez ids. The intersection has',length(venn[[2]]),'elements.',sep=' '))
		cat('\nMake sure this is ok.\n\n')

	}

	# pipeline specific checks ?	
	cat('Checks done.\n')

}




#' Save eset to DB
#' 
#'
#' @title Save eset to DB
#' @param e ExpressionSet object
#' @param path path to the db
#' @author Lefebvre F
#' @export
commitEset <- function(eset,path=db.path)
{
	save(eset,file=file.path(path,'eset.RData'))
	write.csv(exprs(eset),file=file.path(path,'expressions.csv'))
}


#' Annotate an expression set object from the 'database', i.e. pdata and fdata
#' 
#'
#' @title Annotate an ExpressionSet object
#' @param path path to the db
#' @param e ExpressionSet object to annotate
#' @author Lefebvre F
#' @export
annotateEset <-function(e,path=db.path)
{
	pdata  = getPdata(path)	 # this implicitely does checks on primary key and mandatory columns...
	params = getParams(path)
	fdata  = getFdata(alias = params[['platform.alias']])
	
	fData(e) = fdata[ featureNames(e)  ,]
	pData(e) = pdata[ sampleNames(e),]


	if('flagSum' %in% names(attributes(e))) # if present, add to annotation
	{
		fData(e) = cbind(fData(e),attr(e,'flagSum')[featureNames(e),])
	}


	return(e)		
}



#' Retrive annotated eset
#' 
#'
#' @title Retrieve eset from DB
#' @param path path to the db
#' @author Lefebvre F
#' @export
getEset <- function(path=db.path)
{
	load(file.path(path,'eset.RData'))
	eset = annotateEset(eset,path)



	return(eset)	
}



#' Retrieve diff analysis object from DB
#' 
#'
#' @title Retrieve diff analysis object from DB
#' @param path path to the db
#' @author Lefebvre F
#' @export
getFits <- function(path=db.path)
{
	load(file.path(path,'fits.RData'))
	return(fits)
	
}



#' Add a figure to 'database'.
#'
#'
#' title Commit Figure to data base
#' @author Lefebvre F
#' @export
commitFigure <- function(fn, description, path=db.path, type='exploratory')
{
	if(!type %in% c('qc','exploratory')){stop('type must be qc or exploratory')}
	file.copy(from=fn,to=file.path(path, type, basename(fn) )  ,overwrite=TRUE)
	load( file.path(path, type, 'index.RData') , envir=environment() )
	index$index.html = rbind(index$index.html,
		data.frame(
			 'Index'	= 1+nrow(index$index.html)
			,'Description'	= description
			,'File'		= hwrite( gsub('.*\\.','', basename(fn) )  ,link=file.path(path, type, basename(fn) ) )
		)
	)
 	save(index,file=file.path(path, type, 'index.RData'))	
}
# TODO: the index/commit mechanism is convoluted!!! need to simplify... Also, the 
# pipeline itselft could use the commit function instead of building the index object...






#' Run pipeline on the fly
#' 
#'
#' @title Express Run
#' @author Lefebvre F
#' @export
express <- function()
{
	checkSanity()
	raw = loadRaw()
	raw = qualityControl(raw)
	preProcess(raw)
	exploratoryAnalysis()
	differentialAnalysis()
	createReport()
}













                






