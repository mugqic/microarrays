

#' This function implements SOP exploratory analysis. Note that additional figures can be genrated outside of the SOP and still be reported
#' 
#' @title SOP differential analysis
#' @param path db.path
#' @param eset ExpressionSet Object
#' @param variables covariates to enter the linear model. Leave NULL to run default ANOVA\
#' @param coefs formula on original coefficients to test
#' @param design design matrix. Supply this if willing to fit a different model that independent
#' @param model.name Name for the linear model. E.g. "Paired, with gender and trratment as covariates".
#' @param ... Arguments passed down to eBayes()
#' @author Lefebvre F
#' @export 
differentialAnalysis <- function(path=db.path,eset=NULL
			,variables=NULL,coefs=NULL,design=NULL,model.name='Statistical Modeling',params=NULL,de.path=NULL,append_exprs=FALSE,
			append_orig_coefs=FALSE,...)
{
# path=db.path;eset=NULL;variables=NULL;design=NULL;contrasts=NULL;model.name='Statistical Modeling'

	# Prepare dir
	if( is.null(de.path) ){
	  de.path = file.path(path,'differential')
	}
	
	unlink(de.path,recursive=TRUE,force=TRUE)# cleanup
	dir.create(de.path,showWarnings=FALSE, recursive=TRUE)
	
	# Load the database content
	if( is.null(params) ){
		params   = getParams(path)
	}

	# Load the eset if not specified
	if(is.null(eset)){eset = getEset(path)}# by default stats are done on all samples and all genes

	# now design and variables are used to generate the fit. If variables is null, then default.
	if(is.null(variables)) # not specifying the variables triggers default behavior, i,e. independent categorical
	{
		variables = params[['dgea.variables']]
		bls       = params[['dgea.variables.base.levels']] 
		fit       = dgLimmaCategoricalFit(eset, variables)
		if(is.null(coefs))
		{
			if(length(variables)==1)
			{
				coefs  = dgANOVAContrasts(eset, variables, f1 = variables, f1.baseline = bls)
			}else{
				coefs  = dgANOVAContrasts(eset, variables
				, f1 = variables[length(variables)], f1.baseline = bls[length(variables)]
				, f2 = variables[length(variables)-1], f2.baseline = bls[length(variables)-1])
				coefs = as.character(unlist(coefs))
			}
		}
		model.name = paste('Using ',paste(variables,collapse=', '),' as variables and assuming random sampling (no pairing)',sep='')	
	}else{
		fit      = dgLimmaCategoricalFit(eset, variables,design=design)
	}# by default stats are done on all samples and all genes
	design      = fit$design
	cont.matrix = makeContrasts(contrasts = coefs, levels = design)
    	fit2        = contrasts.fit(fit, cont.matrix)
        fit2        = eBayes(fit2, ...)
	fit2$cont.matrix = cont.matrix

	# replication
	replication = getReplication(fit2, output.dir=de.path,lm.name=model.name, css.link="hwriter.css", ret=TRUE, prefix='replication')

	# analysis output
	
	analysis.output = analysisOutput(fit2, output.dir = de.path, ret = TRUE, 
		eset = if(append_exprs){eset}else{NULL},
		to_cbind=if(append_orig_coefs){fit$coefficients}else{NULL}
		) 


	# Initialize summary table
	summary = data.frame('index'=c(1:ncol(fit2)),'coef'=colnames(fit2),row.names=colnames(fit2))	

	# counts : nominal p and FC (p and FC only, FDR only)
	count = fit.count(fit2, tt.args.list=list(
	 'count1'= list('adjust.method'='none','p.value'=params[["count.p"]],'lfc'=log2(params[["count.fc"]]))
	,'count2'  = list('adjust.method'='BH','p.value'=params[["count.fdr"]],'lfc'=log2(1)))
        ,nominal.p=params[["count.p"]],one.table=TRUE, collapse.by = NULL)
	summary = cbind(summary,count[rownames(summary),c('count1','count2')])

	
	# topTables
	tops = topTables(fit2, cont.aliases = summary$index, output.dir = de.path, 
   		html.n.max = params[["html.top.tables.n"]], p.value = params[["count.p"]] ) 
	summary[['top.html']] = file.path(de.path,paste('top_',summary$index,'.html',sep=''))
	summary[['top.csv']]  = file.path(de.path,paste('top_',summary$index,'.csv',sep=''))


	# heatmaps
	fit.heatmap(fit2,eset, coefs.aliases = summary$index, output.dir = de.path
		,nominal.p   =  params[["count.p"]],number  = params[['top.heatmaps.n']]
		,row.labels = 'SYMBOL' ,col.labels  = params[['sample.description.string']]
		,scale='row',remove.dup.row.labels=TRUE)
	summary[['top.heatmap']]  = file.path(de.path,paste('top_heatmap_',summary$index,'.pdf',sep=''))

	
	# basic GSEA # TODO: roast is still dubious.. replace this by Fisher's test as IPA...
	if(!is.null(params[['gene.sets.db.alias']]))
	{
		gene.sets = getGeneSets(params[["gene.sets.db.alias"]],as.list = FALSE)
		gsea = my.romer(eset, design, contrasts=colnames(fit2),method = c('romer','mroast')[2]
			,gene.sets= gene.sets, e.by="ENTREZID", nrot=1000
			,edge.lfc=params[['count.fc']], edge.p.value=params[['count.p']]
			,edge.adjust.method="none", mc.cores=params[['mc.cores']])
		# gsea = temp	
		gsea =  lapply(gsea,function(df){
				df = df[order(df[['Mixed']],decreasing=FALSE),]
				colnames(df)[1:3] = paste('P',colnames(df)[1:3],sep='.')
				df = df[, c('GeneSetID','Name', setdiff(colnames(df),c('GeneSetID','Name')) )  ]
		})
		summary$gsea = sapply(rownames(summary),function(coef){
				df = gsea[[coef]]
				fn = file.path(de.path,paste('gsea_',summary[coef,'index'],'.csv',sep=''))
				write.csv(df,file=fn,row.names=FALSE)
				fn
		},simplify=FALSE)		
	}else{
		gsea = NULL
		summary$gsea = ''
	}

 

	### Prepare the html table in advance....
	summary.html = summary
	
	# Format
	summary.html[['top.html']] = hwrite('html',link=summary.html[['top.html']])
	summary.html[['top.csv']] = hwrite('csv',link=summary.html[['top.csv']])
	summary.html[['top.heatmap']] = hwrite('pdf',link=summary.html[['top.heatmap']])
	summary.html[['gsea']] = hwrite('csv',link=summary.html[['gsea']])   

	# Rename to prettier columns
	colnames(summary.html) = c('Index','Tested Coefficient'
		,paste('Nb. of Probes<br>P<',params[['count.p']],' and |FC|>',params[['count.fc']],sep='')
		,paste('Nb. of Probes<br>P<',params[['count.p']],' at ',100*params[['count.fdr']],'% BH-FDR',sep='')
		,paste('Top Genes List<br>(top ',params[["html.top.tables.n"]],')',sep='')
		,paste('Top Genes List<br>(P<',params[["count.p"]],')',sep='')
		,paste('Heatmap of top<br>',params[["top.heatmaps.n"]],' Probes',sep='')
		,'Gene Sets/Pathways'
		)
    
	# Just for fun, write right away to html
	summary.html.gvis <- gvisTable(summary.html, 
                  options = list(width = 1200, height = 650, allowHtml=TRUE,
                                  page = "enable",
                                  pageSize = nrow(summary.html)))
	print(summary.html.gvis,file=file.path(de.path,'summary.html'))



	fits = list('fit'=fit,'fit2'=fit2,'design'=fit$design,'cont.matrix'=cont.matrix,'model.name'=model.name,
		'variables'=variables,'summary'=summary,'analysis.output'=analysis.output,'tops'=tops,
		'count'=count,'replication'=replication,'summary.html'=summary.html
	,'summary.html.gvis'=summary.html.gvis,'gsea'=gsea)
	

	# commit.fits
	save(fits,file=file.path(path,'fits.RData'))

	return(fits)		

}
# TODO: the default pipeline will not handle numerical covariates well
 
# TODO:
# test package, gsea, apply same concept of summary to exploratory and qc.

# TODO: gsea romer or roast blow











#' The following function implements two of the most common designs : 
#'  - interaction terms of a two way ANOVA 
#'  - one-way ANOVA comparison to a baseline.
#' 
#' dgLimmaCategoricalFit() is a wrapper for lmFit(). It will fit the model ~variables, e.g. ~Mutation*Treatment. One useful capability is the possiblility to supply with a custom design matrix, argument design=design. This allows the fit of any linear model, even one with one or more continous variables.
#' @title Categorical Linear Fits for DGEA
#' @param x An ExpressionSet. (eg. eset)
#' @param variables A character list of Factors from pData(x) used to fit the model. (eg. c("Tissue","Vaccine"))
#' @param dupcor.block A character string corresponding to the Factor in pData(x) used for duplicate correlation mixed modeling. (eg. "Leuka")
#' @param fixed.effect.block A character string corresponding to the Factor in pData(x) used fixed-effect blocking. (eg. "Leuka" or "batch")
#' @param design in case one wants to provide with custom design matrix.
#' @return a fit object.
#' @note Make sure yo know what you are doing when using the block arguments. For complex designs it is better to create the design matrix outside of the function and pass it as an argument.
#' The output values is essentially a fit object with the slot fit$variables. This slot is expected by reporting functions. Nothing stops anyone from creating their own fit,fit2 objects as long as the required slots are populated.
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
dgLimmaCategoricalFit <- function (x, variables, dupcor.block = NULL, fixed.effect.block = NULL, 
    design = NULL) 
{
    if (any(!variables %in% varLabels(x))) {
        stop("Some element in variables is not present in the pData")
    }
    if ((!is.null(dupcor.block)) & (!is.null(fixed.effect.block))) {
        warning("Both random and fixed effect blocking. Are you sure of this?")
    }
    param = attr(x, "param")
    if (is.null(design)) {
        Class = factor(apply(pData(x)[, variables, drop = FALSE], 
            MARGIN = 1, function(ro) paste(ro, collapse = "_")))
        if (!is.null(fixed.effect.block)) {
            block = as.factor(pData(x)[[fixed.effect.block]])
            design <- model.matrix(~-1 + Class + block)
            colnames(design) = gsub("(^Class|^block)", "", colnames(design), 
                perl = TRUE)
        }
        else {
            design = model.matrix(~-1 + Class)
            colnames(design) = gsub("^Class", "", colnames(design), 
                perl = TRUE)
        }
        rownames(design) = sampleNames(x)
        if (!is.null(dupcor.block)) {
            print("Running duplicate Correlation....")
            dupcor = duplicateCorrelation(exprs(x), design = design, 
                block = as.factor(pData(x)[[dupcor.block]]))
            fit <- lmFit(x, block = as.factor(pData(x)[[dupcor.block]]), 
                design, correlation = dupcor$consensus)
            fit$dupcor = dupcor
            fit$dupcor.block = dupcor.block
        }
        else {
            fit <- lmFit(x, design)
        }
    }
    else {
        rownames(design) = sampleNames(x)
        fit <- lmFit(x, design)
    }
    fit$variables = pData(x)[, variables, drop = FALSE]
    fit$TimeStamp = Sys.time()
    fit$author = param$author
    return(fit)
}




#' This function generates a fit2 object... 
#'
#' dgLimmaContrastsFit() is a wrapper for contrasts.fit and eBayes. It also sets some attributes essential for the 
#' reporting part. In addition it performs gsea (experimental). Since dgLimmaContrastsFit return a single MArrayLM object,
#' it's output is set as one element of fits2, but dgLimmaContrastsFit could be called iteratively on many sets of 
#' contrasts using sapply for instance...
#' @title Fit a group of contrasts to a linear model
#' @param fit a fit object, namely the output from dgLimmaCategoricalFit.
#' @param vecCont A list of contrasts taken from the output of dgANOVAContrasts.
#' @param ebayes Whether ebayes should be applied. USeful to get contrasts coef when no resid df
#' @return coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
dgLimmaContrastsFit<-function(fit, vecCont, ebayes = TRUE) 
{
    cont.matrix = makeContrasts(contrasts = vecCont, levels = fit$design)
    fit2 = contrasts.fit(fit, cont.matrix)
    if (ebayes) {
        fit2 = eBayes(fit2)
    }
    else {
        return(fit2)
    }
    fit2$cont.matrix = cont.matrix   
    return(fit2)
}


#' This function writes the content of a fit object to a file with column names formatted for the macro: P. Padj. FC.
#' 
#' @title Summary File of a diff. exprs. analysis
#' @param lm MArrayLM object (fit2)
#' @param output.dir where the xls should be written
#' @param ret coming soon
#' @param cont.aliases coming soon
#' @param probes.subset coming soon
#' @param fn coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
analysisOutput <- function(lm, output.dir = ".", ret = TRUE, 
	cont.aliases = colnames(lm), probes.subset = NULL, fn = "analysis_output.csv", eset=NULL, to_cbind=NULL) 
{
    pvals = lm$p.value
    colnames(pvals) = paste("P.", cont.aliases, sep = "")
    logfcs = lm$coefficient
    colnames(logfcs) = paste("logFC.", cont.aliases, sep = "")
    adjpvals = apply(pvals, MARGIN = 2, multtest.BH)
    colnames(adjpvals) = paste("Padj.", cont.aliases, sep = "")
    fcs = apply(lm$coefficients, MARGIN = 2, toFold)
    colnames(fcs) = paste("FC.", cont.aliases, sep = "")
    analysis.output = data.frame(pvals, adjpvals, fcs, logfcs, lm$genes, 
        check.names = FALSE, stringsAsFactors = FALSE)
    if (!is.null(probes.subset)) {
        analysis.output = analysis.output[probes.subset, ]
    }
	
	
	
		# Join with eset if required
		if(!is.null(to_cbind))
		{
			analysis.output = cbind(analysis.output,to_cbind[ analysis.output[["ProbeID"]],])
		}
	
	# Join with eset if required
	if(!is.null(eset))
	{
		analysis.output = cbind(analysis.output,exprs(eset)[ analysis.output[["ProbeID"]],])
	}
	
	# Write and return
    write.csv(analysis.output, file = file.path(output.dir, fn), 
        row.names = TRUE)
    if (ret) {
        return(analysis.output)
    }
    else {
        return(NULL)
    }
}




#' This funcion accepts an MArrayLM object, writes (xls,html) and returns well formatted topTables
#' 
#' @title Top Tables
#' @param lm coming soon
#' @param cont.aliases coming soon
#' @param output.dir coming soon
#' @param html.n.max coming soon
#' @param p.value A p-value threshold to limit the size of the csv table...
#' @param ret coming soon
#' @note lm$genes has to have controlled column names SYMBOL and PRobeID
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
topTables<-function(lm, cont.aliases = colnames(lm), output.dir = ".", 
    html.n.max = 1000, p.value = 1, ret = TRUE) 
{
    tts = sapply(colnames(lm), function(cont) {
        topTable(lm, coef = cont, number = nrow(lm), adjust.method = "BH")
    }, simplify = FALSE)
#   tts$F = topTableF(lm, number = nrow(lm), adjust.method = "BH")
#    tts$F = tts$F[, -1 * (1 + ncol(lm$genes)):(ncol(lm$genes) + 
#        ncol(lm))]
    tts = lapply(tts, function(top) {
        top = top[, c("ProbeID", "SYMBOL", setdiff(colnames(top), 
            make.names(colnames(lm$genes))), setdiff(make.names(colnames(lm$genes)), 
            c("ProbeID", "SYMBOL")))]
        if ("logFC" %in% colnames(top)) {
            top$logFC = toFold(top$logFC)
            colnames(top) = gsub("logFC", "Fold Change", colnames(top))
            top$AveExpr = abs(top[["Fold Change"]])
            colnames(top) = gsub("AveExpr", "|Fold Change|", 
                colnames(top))
        }
        top$AveExpr = NULL
        top$B = NULL
        return(top)
    })
    names(tts) = c(cont.aliases)#, "F")
    for (ct in names(tts)) {
        top = tts[[ct]]
        write.csv(top[top$P.Value <= p.value, ], file = file.path(output.dir, 
            paste("top_", ct, ".csv", sep = "")), row.names = FALSE)
        sortable.html.table(top[1:min(html.n.max, nrow(top)), 
            ], paste("top_", ct, ".html", sep = ""), output.dir, 
            colnames(lm)[cont.aliases==ct])
    }
    if (ret) {
        return(tts)
    }
    else {
        return(NULL)
    }
}








#' This function retrives the arrays involved in contrast explicited in a fit object.
#' coming soon
#' @title Retrieve arrays involved in a contrast
#' @param lm fit object with the contrast matrix used... generaetd by the pipeline
#' @param coefs Name of the coefficients (contrasts)
#' @param cont.matrix coming soon
#' @return A list of character vectors listing arrays involved in the coeffficients estimation
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
retrieve.involved.arrays <- function(lm,coefs = colnames(lm),cont.matrix   = NULL )
{	
	if(is.null(cont.matrix))
	{
		if( ! is.null(lm$cont.matrix) )
		{
			cont.matrix = lm$cont.matrix
		}else{stop('cont.matrix is NULL and cannot be found listed in the  fit object. This matrix is necessary to retrive arrays involved in a given contrast. Provide it as argument.')}
	}

	if(any(! coefs %in% colnames(cont.matrix))){stop('coefs do not match colnames of cont.matrix')}
	if(any(! rownames(cont.matrix) %in% colnames(lm$design) )){stop('coefs in cont.matrix not consistent with design mat')}
	#if(any(! rownames(lm$design) %in% sampleNames(e) )){
	#stop('Some rownames of design cannot be matched to sampleNames(e). Perhaps rownames were not assigned as sampleNames(eset) after running model.matrix?')}

	l = sapply(coefs,function(coef)
	{
		initial.coefs   = names(which(cont.matrix[,coef]!=0))  # This extracts the Classes involved in a contrast
		sub.design      = lm$design[,initial.coefs,drop=FALSE] # This allows finding arrays involved in the contrast and their class
		involved.arrays = rownames(	sub.design  )[    apply(sub.design,MARGIN=1,function(ro){any(ro!=0)})    ]	
		involved.arrays
	},simplify=FALSE)	
	return(l)
}








#' This function takes a fit object and counts the number of significant genes at various thresholds
#' 
#' coming soon
#' @title Count the number of differentially expressed genes
#' @param lm A fit (MArrayLM) object.
#' @param coefs Character vector of coefficient formulas
#' @param A list of parameters for diff. expression
#' @param nominal.p Additional constraint on unadjusted p-value
#' @param collapse.by Column name from lm$genes that should be used to collapse probes
#' @param one.table one.table : very often one, e.g. ppt does not need to distingish up and down, and does not need the F-test. one.table=TRUE returns a single data.frame with coefs as rows and thresholds as columns conviniently named.
#' @return A list of data.frames or a single table listing the numbers of DE genes
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
fit.count <- function(lm,coefs=colnames(lm), tt.args.list=list(

'Stringeant (P<0.01, FDR<5% with |FC|>2)'= list('adjust.method'='BH','p.value'=0.05,'lfc'=log2(2))
,'Medium (P<0.01, FDR<5% with |FC|>1.3)' = list('adjust.method'='BH','p.value'=0.05,'lfc'=log2(1.3))
,'Relaxed (P<0.01, FDR<25% with |FC|>1.3)' = list('adjust.method'='BH', 'p.value'=0.25,'lfc'=log2(1.3)))
,nominal.p=0.01,collapse.by=NULL,one.table=FALSE)
{
	# Checks
	if(is.null(collapse.by))
	{
		lm$genes[['ID']] = rownames(lm)
		collapse.by = 'ID'
	}else{
		if( ! collapse.by %in% colnames(lm$genes)  ){stop('collapse.by not in lm$genes')}
		lm$genes[['ID']] = lm$genes[[collapse.by]]
	}
	
	# Check coefs
	if(is.logical(coefs)){coefs=which(coefs)}	
	if(is.numeric(coefs)){coefs=colnames(lm)[coefs]}
	if(any(! coefs %in% colnames(lm))){stop('Invalid coefs specified')}	
	if(!setequal(colnames(lm),coefs)){print('Have to subset lm...');lm = lm[,coefs]} # because it takes a damn long time

	ndegs = sapply(tt.args.list,function(tt.args)
	{
		counts = sapply(coefs,function(coef){
				top = topTable(lm,coef=coef,number=Inf, p.value=tt.args$p.value, lfc=tt.args$lfc
								, adjust.method=tt.args$adjust.method)	
					top = top[!duplicated(top[['ID']]),]
					top = top[top[['P.Value']] <= nominal.p,]
					c('Both'=nrow(top),'Up'=nrow(top[top[['t']]>0,]),'Down'=nrow(top[top[['t']]<0,]))
		},simplify=FALSE)
		topF = topTableF(lm,number=Inf,p.value=tt.args$p.value,adjust.method=tt.args$adjust.method)
		topF = topF[!duplicated(topF[['ID']]),]
		topF = topF[topF[['P.Value']] <= nominal.p,]
		counts[['F-Test']] = c('Both'=nrow(topF),'Up'=NA,'Down'=NA)					
		return(data.frame(t(data.frame(counts,check.names=FALSE)),check.names=FALSE))
	},simplify=FALSE)

	if(one.table)
	{
		ndegs = melt(lapply(ndegs,function(df)
		{
			df[['Coefficient']] = rownames(df)
			df =  df[rownames(df)!='F-Test',c('Coefficient','Both')] 
		}))[,c(1,3,4)]
		ndegs = cast(ndegs,Coefficient~L1,value='value')
		rownames(ndegs) = ndegs$Coefficient
		ndegs = ndegs[colnames(lm),c('Coefficient',names(tt.args.list))]
		class(ndegs) =  "data.frame"
	}

	return(ndegs)
}
# TODO: Roxygen has trouble parsing tt.list.args correctly...















#' This function takes a fit object and plots the heatmap of the top genes.
#' 
#' It runs topTable on each contrast (and topTableF), then plots the samples involeed in the contrast. This high-level wrapper will perform a bunch of very common operations on a fit object.
#' @title Top genes heatmaps
#' @param lm MArrayLM (fit) object. If cont.matrix is not listed within this object (dgLimmaContrastFit does), argument cont.matrix must be supplied. This is necessary to retrive arrays involved in coefs.
#' @param e ExpressionSet object. Must have varLabels and fvarLabels specicied in pheatmap.eset.dots (row.labels,cols.ann,col.labels)
#' @param coefs contrast formulas one or many in colnames(lm)
#' @param coefs.aliases contrasts have large names...
#' @param output.dir Path to where heatmap files are written
#' @param cont.matrix The contrast matrix. The pipeline stores them in a lm$cont.matrix. Must otherwise be supplied. 
#' @param adjust.method c.f. topTable()
#' @param p.value c.f. topTable()
#' @param nominal.p as a safeguard
#' @param lfc c.f. topTable()
#' @param number c.f. topTable()
#' @param sort.by c.f. topTable()
#' @param resort.by c.f. topTable()
#' @param row.labels fvarLabels used as row labels in the heatmap
#' @param remove.dup.row.labels Should rows with same row.labels as more significant rows be omitted
#' @param col.labels varLabels used as column labels in the heatmap
#' @param cols.ann Which varLabels(e) should be put as color annotation at the top of the heatmap
#' @param \dots arguments to be passed down to pheatmap e.g. scale, breaks, color, etc.
#' @return fit object augmented with a lot of stuff
#' @note coming soon
#' @author Lefebvre F
#' @seealso pheatmap.wrapper(), pheatmap.eset(), topTable()
#' @references coming soon
#' @keywords coming soon
#' @export
fit.heatmap <- function(lm,e,coefs   = colnames(lm),
	coefs.aliases = sapply(colnames(lm),function(x)x,simplify=TRUE),
	output.dir  = 'heatmaps',cont.matrix  = NULL,adjust.method = 'BH',p.value  = 1,nominal.p   = 1, 
	lfc  = log2(1),number  = 50,sort.by  = "B",resort.by  = NULL,		
	row.labels = NULL ,remove.dup.row.labels = FALSE,col.labels  = NULL,
	cols.ann  = colnames(lm$variables)	,...)
{
	# Checks and preparation
	if(!file.exists(output.dir)){dir.create(output.dir)}
	if(is.null(cont.matrix) & is.null(lm$cont.matrix)){stop('cont.matrix absent from fit object or as an argument')}
	if(is.logical(coefs)){coefs=which(coefs)}	
	if(is.numeric(coefs)){coefs=colnames(lm)[coefs]}	

	names(coefs.aliases) = coefs

	# Retrieve the arrays involved
	arrays  = retrieve.involved.arrays(lm,coefs=coefs, cont.matrix   = cont.matrix)
	
	# Retrieve the top genes
	lm$genes[['ProbeID']] = rownames(lm) # becasue we can't assume fit$genes is populated
	tops = sapply(coefs,function(coef)
	{
		top = topTable(lm, coef=coef, adjust.method=adjust.method, p.value=p.value, lfc=lfc, 
				number=number,sort.by=sort.by, resort.by=resort.by)	
		top = top[top$P.Value<=nominal.p,]
		#print(paste(coef,': ',max(top$adj.P.Val),sep=''))
		return(top)	
	},simplify=FALSE)

	
	# Now plot the heatmaps
	heatmaps = sapply(names(tops),function(coef){
			if( nrow(tops[[coef]])<2 | length(arrays[[coef]]) <2     )
			{
				warning('Cannot plot heatmap. Need at least 2 rows and two columns ')
				pdf(file=file.path(output.dir
						,paste('top_heatmap_',   gsub('\\/','_DIV_',coefs.aliases[coef]),'.pdf',sep='')),height=2,width=2);
				plot.new();dev.off();
				return(e[nrow(tops[[coef]][['ProbeID']]), length(arrays[[coef]])])
			}

			pheatmap.eset(
					 e                     = e
					,file                  = 
								file.path(output.dir
							,paste('top_heatmap_', gsub('\\/','_DIV_',coefs.aliases[coef]),'.pdf',sep=''))
					,which.rows            = featureNames(e)[ featureNames(e) %in% tops[[coef]][['ProbeID']] ]
					,row.labels            = row.labels
					,remove.dup.row.labels = remove.dup.row.labels
					,which.cols            = sampleNames(e)[sampleNames(e) %in%  arrays[[coef]] ]
					,col.labels            = col.labels
					,cols.ann              = cols.ann
					,... )
	},simplify=FALSE)



}



#' This function takes a fit object and writes the replication to xls and html. The Fit object HAS to have slot "variables".
#' 
#' @title Replication levels of an analysis
#' @param lm MArrayLM object (fit)
#' @param output.dir where the xls and html should be written
#' @param lm.name coming soon
#' @param css.link the relative location of the css file (hwriter) that should be written in the html file.
#' @param ret should the data.frame be returned
#' @param prefix prefix to the output file names
#' @return Writes to output.dir/replication.html/.xls
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
getReplication <- function(lm, output.dir=".", lm.name="lm", css.link="hwriter.css"
						, ret=FALSE, prefix='replication')
{

		# suppressWarnings(as.numeric(c('a','1')))

		# Check variables slot
		if(is.null(lm$variables))
		{stop("Current implementation of getReplication requires lm object to have non-standard slot \"variables\" ")}


		#  Deal with numerical covariates
		variables = lm$variables
		isCat = sapply(variables,function(co){  any(is.na(  suppressWarnings(as.numeric(co))   ))     })
		variables[,!isCat] = 'Numerical'

		# Create replication xls and html files
		Replication           = as.data.frame(melt(table(variables))) #  data.frame("Replication"=apply(f$fit$design,sum,MARGIN=2)) 
		Replication           = Replication[order(Replication$value,decreasing=TRUE),];
		rownames(Replication) = c(1:nrow(Replication))
		if(ncol(Replication)==2)
		{
			colnames(Replication)[1]=colnames(variables)
		}
		colnames(Replication)[ncol(Replication)] = "Replication"
		WriteXLS("Replication", file.path(output.dir,paste(prefix,'.xls',sep=''))
					, row.names =TRUE, AdjWidth = TRUE,BoldHeaderRow = TRUE)
		p=openPage(file.path(output.dir,paste(prefix,'.html',sep='')),link.css=css.link,title=paste("Replication for model",lm.name))
		hwrite(paste("Replication for model",lm.name), p, heading=3,center=TRUE)
		hwrite(Replication,p,row.names=TRUE,row.bgcolor='#ddaaff',row.style=list('font-weight:bold'),br=TRUE)
		closePage(p)
		if(ret){return(Replication)}else{return(NULL)}
}



