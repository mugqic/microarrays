

#' This function loads the raw data from the 'database', i.e it reads chips, annotates it's rows
#' and its columns. It saves a cached version of the R object in case we simply want to re-load
#' a previously read object without reading and annotating it again. (useful for large datasets)
#'
#' @title Load the raw intensities data
#' @param path path to where the skeleton of the db should be created
#' @author Lefebvre F
#' @export
loadRaw <- function(path=db.path, peek=FALSE)
{
	# Hard-coded mapping between pipelines and types
	types = c('AgilentGeneExpression'='agilent','Affy3IVT'='affy','Affy_miRNA'='affy','AgilentmiRNA'='agilentmiRNA','IlluminaHumanMethylation450'='450k',
			 'AgilentMeDIP'='agilent2color', 'IlluminaExpressionBeadChip'='illmnExpBeadChip','AffyGeneST'='affyGeneST',
			 "taqman_miRNA"="taqman", 'AffyClariom'="affyClariom")

	# Load the database content
	params   = getParams(path)
	pdata    = getPdata(path)
	fdata    = getFdata(alias=params[['platform.alias']])
	pipeline = getSupportedPlatforms()[params[['platform.alias']],'pipeline']

	if(peek){pdata=pdata[1:3,]}

	# Call the necessary method # TOODO: more advanced R with methods would be nice, alas
	raw = eval(call(name=paste('loadRaw',types[[pipeline]],sep='.'),pdata,fdata))

	return( raw  )
}


#' Read Viia7 taqman data
#'
#'
#' @title load Viia7 taqman data
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.taqman <- function(pdata,fdata)
{

	raw = readViia7( files = unique(pdata[["FilePath"]]) )

	# Check and subset on chips of interest
	if(any( ! rownames(pdata) %in% colnames(raw)  )){stop('ArrayIDs not all in raw data')}
	raw = raw[,rownames(pdata)]

	# Check features???
	if(any( ! rownames(raw)   %in%  rownames(fdata) )){stop('raw data miRNAs not all in annotation!')}

	# Sample Annotaions
	pData(raw) = cbind( pdata[sampleNames(raw),] , pData(raw))

	# Annotate the features better
	fData(raw) = cbind( fdata[featureNames(raw),] , fData(raw))

	return(raw)
}






#' Read Affymetrix CEL files with the oligo package
#'
#'
#' @title load Affymetrix GeneST chips
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.affyGeneST <- function(pdata,fdata)
{
	# Read raw
	raw = oligo::read.celfiles(filenames=pdata$FilePath, sampleNames=pdata$ArrayID)
	pData(raw) =  pdata[sampleNames(raw),]

	# Test feautres...
	temp = oligo::rma(raw,normalize=FALSE,background=FALSE)
	if(!setequal(rownames(fdata), featureNames(temp) )){warning('probe annot is not exactly same as CEL content check this')}

	return(raw)
}

#Eloi Mercier
loadRaw.affyClariom=loadRaw.affyGeneST





#' Read Illumina's Expression Bead Chips
#'
#'
#' @title load Illlumina Expression Bead Chips
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.illmnExpBeadChip <- function(pdata,fdata)
{

	raw =  read.ilmn(
		files=  unique(pdata$FilePath)
		,ctrlfiles= gsub('sample_probe_report.txt','control_probe_report.txt',unique(pdata$FilePath))
#		,path=NULL
#		,ctrlpath=NULL

		,probeid="ProbeID"
		,annotation=c("TargetID")
		,expr=":AVG_Signal"

#		,other.columns="Detection"

#		,sep="\t"
		,quote=""
#		,verbose=TRUE
#		,...
		)
	rownames(raw$targets) = raw$targets[['SampleNames']]


	# Check and subset on chips of interest
	if(any( ! rownames(pdata) %in% colnames(raw)  )){stop('ArrayIDs not all in raw data')}
	raw = raw[,rownames(pdata)]

	# Check features???
	if(any( ! rownames(fdata) %in% rownames(raw)  )){stop('ProbeIDs not all in raw data')}

	return(raw)

}







#' Read Agilent chips, annotate the resulting EListRaw from the annotation provided
#'
#'
#' @title load Agilent chips
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.agilent2color <- function(pdata,fdata)
{
	# Read raw
	targets = pdata
	targets[['FileName']] = targets[['FilePath']]
	raw = read.maimages(targets, source="agilent",names=rownames(targets))

	# Annotate features
	if(!setequal(rownames(fdata),raw$genes[['ProbeName']])){stop('Error in loadRaw.agilent. raw data feature names do not exactly match that of the annotation')}
	rn = rownames(raw$genes)
	raw$genes = cbind(raw$genes,fdata[raw$genes[['ProbeName']],])
	rownames(raw$genes) = rn

	# Annotate columns (already done by read.maimages)
	# ...

	return(raw)
}



#' Read Illumina 450k Methylation data
#'
#'
#'
#' @title load 450k data
#' @param pdata pdata data.frame annotating arrays. FilePath column shoud have either idat Green/Red separated by comma,
#'   or individual Genome studio files. If using idat, provide with the name of the Green channel...
#' @param fdata fdata annotating probes
#' @return RGSet object from minfi package???? or mset????
#' @author Lefebvre F
#' @export
loadRaw.450k <- function(pdata,fdata)
{

	# guess format : idat of Genome Studio files (nor-normalized)????
	# if Genoem Studio, should outputy a warning saying they should not have been bkg corrected or whatever

	# fdata = getFdata() ; pdata = getPdata()
	require(minfi)
	pdata[['_basepath']]	= gsub('_Grn\\.idat','',pdata[['FilePath']]) # this'll be the basenames
        rownames(pdata)		= basename(pdata[['_basepath']])
	raw     		= minfi::read.450k(pdata[['_basepath']]	 , verbose=TRUE)
	pData(raw)		= pdata[sampleNames(raw),] # just to make sure, re-order
	sampleNames(raw)	= pData(raw)$ArrayID # re-assign our original ArrayID
	mset =  minfi::preprocessRaw(raw)


	# Check if our annotation is ok
	if( length( setdiff(featureNames(mset),rownames(fdata)) ) > 0 ){stop('Error in loadRaw.450k(). raw data feature names do not exactly match that of the annotation')}

	return(raw)
	# TODO: add support for GenomeStudio files.
}

#' Read Agilent chips, annotate the resulting EListRaw from the annotation provided
#'
#'
#' @title load Agilent chips
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.agilent <- function(pdata,fdata)
{
	# Read raw
	targets = pdata
	targets[['FileName']] = targets[['FilePath']]
	raw = read.maimages(targets, source="agilent",green.only=TRUE,names=rownames(targets), other.columns=c('gIsFeatNonUnifOL','gIsBGNonUnifOL','gIsFeatPopnOL','gIsBGPopnOL'))

	# Annotate features
	if(!setequal(rownames(fdata),raw$genes[['ProbeName']])){stop('Error in loadRaw.agilent. raw data feature names do not exactly match that of the annotation')}
	rn = rownames(raw$genes)
	raw$genes = cbind(raw$genes,fdata[raw$genes[['ProbeName']],])
	rownames(raw$genes) = rn

	# Annotate columns (already done by read.maimages)
	# ...



	# A recurring problem with Agilent are these intense spots... here we perform
	#  a grubbs test on each spot and compute other statistics. Probes with
	# a significant Grubbs test (P<1e-5) and large median are flagged for later use (maybe, ha!)
	print('Running Grubbs tests...')
	m = uA.as.matrix(raw) # This turns into log2 matrix
	gt = mclapply(1:nrow(m),function(i)   { grubbs.test(m[i,])}   ,mc.cores=8)
	gt = data.frame('outlier'=  sapply(gt,function(x){gsub('^G\\.','',names(x$statistic)[1])})
			,'grubbs.p.value'= sapply(gt,function(x){x$p.value}) )
	gt[['grubbs.adj.Pval']] = my.multtest.BH(gt[['grubbs.p.value']])
	gt[['median']] = apply(m,MARGIN=1,median)
	gt[['mad']] = apply(m,MARGIN=1,mad)
	gt[['ol.value']] = mapply(function(ol,i){m[i,ol]},gt[['outlier']],1:nrow(m))
	gt[['ol.median.deviation']] = gt[['ol.value']] - gt[['median']]
	gt[['Row']] = as.numeric(rownames(gt))

	gt[["flag"]] = ifelse(gt[['grubbs.p.value']]< 0.01 & gt[['grubbs.adj.Pval']]<0.75 & abs(gt[['ol.median.deviation']]) > 3, 1,0)
	gt$"outlier" = factor(gt$"outlier",levels=colnames(m))
	attr(raw,'grubbs.test') = gt

	stain = cast(gt,Row~outlier,add.missing=TRUE,fill=0,value="flag")
	stain$Row=NULL; stain=as.matrix(stain);class(stain) = 'matrix'
	stain = stain[rownames(gt),colnames(m)]; rownames(stain) = NULL
	raw[['other']][['SingleSpotOL']] = stain

	return(raw)
}



#' These arrays are loaded identically as expression arrays
#'
#'
#' @title load Agilent chips
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.agilentmiRNA <- function(pdata,fdata)
{
	# Read raw
	targets = pdata
	targets[['FileName']] = targets[['FilePath']]
	raw = read.maimages(targets, source="agilent",green.only=TRUE,names=rownames(targets), other.columns=c('gIsFeatNonUnifOL','gIsBGNonUnifOL','gIsFeatPopnOL','gIsBGPopnOL'))

	return(raw)
}







#' Read Affymetrix CEL files
#'
#'
#' @title load Affymetrix GeneChip
#' @param pdata pdata data.frame annotating arrays
#' @param fdata fdata annotating probes
#' @author Lefebvre F
#' @export
loadRaw.affy <- function(pdata,fdata)
{
	# Read raw
	raw =  ReadAffy(filenames=pdata$FilePath,sampleNames=pdata$ArrayID)
	pData(raw) = pdata[sampleNames(raw),]

	# Annotate features
	if(!setequal(rownames(fdata),unique(probeNames(raw)))){warning('probe annot is not exactly same as CEL content check this')}


	return(raw)
}









#' A function to read the Viaa 7 excel format data files and return a qPCRSet
#' @param files character vector of Viaa excel export
#' @param cycles This is the number of cycles performed. Undertermined are set to this value, and this is used for imputation by nondetects...
#' @param sampleAnnotation character vector of names of columns containing annotation information about the samples
#' @param featureAnnotation character vector of names of columns containing annotation information about the targets
#' @param other.columns character vector of names of other columns to be read containing spot-specific information (NOT IMPLEMENTED read.maimages as inspiration)
#' @param fun.aggregate A function defining the operation to perform in order to collapse obervations from the same target and sample. This function must assume input is numerial and undetermined values are NAs.
#' @export
readViia7 <- function(
	files, cycles = 40, sampleAnnotation = NULL, featureAnnotation = NULL, other.columns = NULL,
	fun.aggregate = function(x){
		x = mean(x,na.rm=TRUE)
		ifelse(is.nan(x),NA,x)
		}
	)
{
	# files = filenames[[2]]; sampleAnnotation = NULL; featureAnnotation = NULL; fun.aggregate = function(x){ x = mean(x,na.rm=TRUE) ; ifelse(is.nan(x),NA,x)}

	message("Reading Viia7 format files...")

	# We want to keep track of the source file
	sampleAnnotation = c(sampleAnnotation,".file")

	# Read the files
	dat = sapply( files , function(fn){
		 #read.xls(fn,skip=13,header=TRUE,stringsAsFactors=FALSE,comment.char='',check.names=FALSE,colClasses='character')
		 df = read.xls(fn,header=FALSE,stringsAsFactors=FALSE,comment.char='',check.names=FALSE,colClasses='character')
		 df = df[ !grepl("^#",df[,1]) ,]
		 colnames(df) = df[1,]
		 df = df[-1,]
		 colnames(df) = gsub("^Ct$","Cт", colnames(df)) # xlsx does not have tau for some reason
		 df[[".file"]] = fn
		 df
	},simplify=FALSE)

	# Check if all complete and consistent
	dat = sapply(dat,function(df){
		#if( ! identical(colnames(df),colnames(dat[[1]]) )  ){
		#	stop("Files have different column names. This isn't supported right now")
		#}
		mandatory.columns = c("Sample Name","Target Name","Cт", sampleAnnotation,featureAnnotation, other.columns )
		if( any( ! mandatory.columns  %in% colnames(df)   ) )
		{
			stop(paste0("Columns ",mandatory.columns,"are absent from the file",collapse=' '))
		}
		df[,c(mandatory.columns)]
	},simplify=FALSE)

	# Append all the tables together
	dat = do.call(rbind,dat)

	# Define controled name columns in preparation for qPCRset
	dat[["featureNames"]] = dat[["Target Name"]] # Annoying necessary to accomodate HTqPCR. people don't seem to know about featureNames()?
	dat[[".sampleNames"]] = dat[["Sample Name"]] # temporary because of the use of formula in cast() below
	dat[["Ct"]] 		  = dat[["Cт"]]

	# Deal with Ct values and featureCategory and flags
	dat[["Ct"]] = suppressWarnings(as.numeric(dat[["Ct"]])) # anything text is set to numerical NA
	exprs = cast(dat, featureNames ~ .sampleNames , value="Ct",fun.aggregate = fun.aggregate, add.missing=FALSE)
	rownames(exprs) = exprs[,1];
	exprs = as.matrix(as.data.frame(exprs[,-1,drop=FALSE]))
	featureCategory = as.data.frame(ifelse(is.na(exprs),"Undetermined","OK"))
	flag            = as.data.frame(ifelse(is.na(exprs),"Flagged","Passed")) # HTqPCRmight change if have mor einfo, e.g. Omit col

	# Prepare the basic feature annotation
	featureData =  unique(dat[,c("featureNames",featureAnnotation),drop=FALSE])
	rownames(featureData) = featureData[["featureNames"]]
	featureData = featureData[rownames(exprs),,drop=FALSE]

	# Prepare the basic sample annotation
	phenoData =  unique(dat[,c(".sampleNames",sampleAnnotation),drop=FALSE])
	rownames(phenoData) = phenoData[[".sampleNames"]]
	phenoData = phenoData[colnames(exprs),,drop=FALSE]

	# Builfd qPCRset object
	raw = new("qPCRset",exprs=exprs, featureCategory=featureCategory, flag=flag)
	featureNames(raw) = rownames(exprs) # this is necessary, unexprected behavior
	pData(raw) = phenoData
	fData(raw) = featureData

	# Finally, replace NAs with the limit
	exprs(raw)[is.na(exprs(raw))] = cycles

	# Correct some ExpressionSet inconsistencies
	rownames(pData(raw)) = sampleNames(raw)
	rownames(fData(raw)) = featureNames(raw)

	return(raw)
}
# Parse the header  # TODO: NOT used for now
# h = read.xls(fn,nrow=13,header=FALSE,stringsAsFactors=FALSE,comment.char='',row.names=1)[,1,drop=FALSE]
# h = lapply( as.data.frame(t(h)), trimWhiteSpace )
# names(h) = gsub("(# *|: *$)","",names(h))
# h[["Efficiency"]]  = data.frame(
# 			"Target Name"=str_extract_all(h[["Efficiency"]],"(  |\\[).*? =")[[1]],
# 			"Effciency" =  str_extract_all(h[["Efficiency"]]," [0-9].*?\\%")[[1]] )
# h[["Efficiency"]][,1]  = gsub("(  |\\[| =$)","",h[["Efficiency"]][,1])
# h[["Efficiency"]][,2]  = as.numeric(gsub("(^ |%$)","",h[["Efficiency"]][,2]))
#         message(paste(c("Unique efficiency values:",unique(h[["Efficiency"]][,2]) ),collapse=' '))
# df[["SourceFile"]] = fn # TODO: NOT used for now
#return(df)
# Parse the actual data table
# TODO do check to insure column names are identical, and perhaps more checks, e.g. geometry, etc

# MAIL authors: about qPCRset not exactly inheritinh problems. E.g. in vignette, flag must be set otherwise cannot subset
# also does not accept rownames, can't instanciate with AnnotatedDayaFrame, etc.


