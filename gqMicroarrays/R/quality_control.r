

#' This function implements SOP quality controls. Note that additional figures can be genrated outside of the SOP and still be reported
#'
#' @title SOP Quality Control
#' @param path db.path
#' @author Lefebvre F
#' @export
qualityControl <- function(obj, path=db.path)
{
# path =  db.path; obj = raw

	# Hard-coded mapping between pipelines and types
	types = c('AgilentGeneExpression'='agilent'
		,'Affy_miRNA'='affy'
		,'Affy3IVT'='affy'
		,'AgilentmiRNA'='agilentmiRNA'
		,'IlluminaHumanMethylation450'='450k'
		,'AgilentMeDIP'='agilent2color'
		,'IlluminaExpressionBeadChip'='illmnExpBeadChip'
		,'AffyGeneST'='affyGeneST'
		,'taqman_miRNA' ="HTqPCR"
		,'AffyClariom'="affyClariom"
	)

	qc.path = file.path(path,'qc')
	unlink(qc.path,recursive=TRUE,force=TRUE)# cleanup
	dir.create(qc.path,showWarnings=FALSE, recursive=TRUE)

	# Load the database content
	params   = getParams(path)
	pipeline = getSupportedPlatforms()[params[['platform.alias']],'pipeline']

	# Call the necessary method # TOODO: more advanced R with methods would be nice, alas
	index = eval(call(name=paste('qualityControl',types[[pipeline]],sep='.'),obj,qc.path))
	# using the commit machanism will make things much simpler... no more index returning and crap

	index = data.frame('Index'=c(1:nrow(index)),index)
	colnames(index)= c('Index','Description','File')
	index.html = index
	index.html[['File']] = hwrite('pdf',link=index.html[['File']])
	index =  list('index'=index,'index.html'=index.html)




	# Outliers removal
	pdata    = getPdata(path)
	outliers = rownames(pdata)[ pdata$outlierFlag != '']
	cat(paste('\nWill remove the following arrays:\n',paste(outliers,collapse=', '),'\n\n',sep=''))

	# Remove
	if( is(obj,'ExpressionSet') | is(obj,'AffyBatch') | is(obj,'RGList')  |
	    is(obj,'RGChannelSet') | is(obj,'GeneFeatureSet') )
	{
		obj = obj[,!sampleNames(obj) %in% outliers]
	} else
	{
		obj  = obj[,!colnames(obj) %in% outliers]
	}

	# Index the removals
	removal.index = pdata[outliers,]
	if(length(outliers)>0)
	{
		removal.index$Index=c(1:nrow(removal.index))
		removal.index = removal.index[,  unique(c('Index','ArrayID','SampleID','outlierFlag'
				,params[['sample.description.string']])) ]
		colnames(removal.index)[4] = 'Reason for Removal'

	}
	index$outliers = removal.index

	save(index,file=file.path(qc.path,'index.RData'))
	return(obj)
}
# TODO: instead of asking the child functions to create index... use the commitFigure mechanism.
# also the path to files ...










#' This function implements SOP quality controls. Note that additional figures can be genrated outside of the SOP and still be reported
#'
#' @title SOP Quality Control Agilent Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.agilentmiRNA <- function(raw,path)
{
	l = list(
		 uA.boxplot(raw,path) # Boxplots
		,uA.cordist.hclust(raw,path) # hclust
		,uA.pairwise.manhattan.hclust(raw,path)# pairwise man heatmap
		,uA.mds(raw,path)# MDS
		,uA.pca(raw,path)# PCA
		)

	# heatmap of control probes
	fn = file.path(path,'heatmap_control_probes.pdf')
	e = raw[raw$genes$ControlType!=0,]
	e <- backgroundCorrect(e, method="none")
	e <- normalizeBetweenArrays(e, method="none")
	e = avereps(e, ID = e$genes[["ProbeName"]])
	pheatmap.wrapper(e$E,fn=fn,scale='none')
	l = c(l,list(data.frame('Description'='Heat map of intensities of control probes','path'=fn)))


	# Chip images
	# > imageplot(log2(RG$Gb[,1]), RG$printer, low="white", high="green")
	if(!is.null(raw$printer)){
	fn = file.path(path,'raw_intensity_images.pdf')
	pdf(fn,width=5,height=5)
	for(s in colnames(raw))
	{
		imageplot(log2(raw$E[,s]), raw$printer, low="white", high="blue",main=s)
		imageplot(log2(raw$Eb[,s]), raw$printer, low="white", high="blue",main=paste0(s, ' (background)'))
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Images of Raw log2 Intensites','path'=fn)))
	}else{warning("raw object does not have slot raw$printer. Image plot skipped. This can be the case for custom arrays where not all spots are included. Would require added code to complete the matrix and add the printer object.")}


	index = do.call(rbind,l) # argh, annoying and unelegant to have to do this.. commitFigure(asap)
}







#' This function implements various QCs on raw unormalized, unimputed qPCRSet object
#'
#' @title SOP Quality Control Illumina beadchip Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.HTqPCR <- function(raw,path)
{
	dir.create(path,showWarnings=F,recursive=T)

	raw.eset = ExpressionSet(assayData=exprs(raw),phenoData=phenoData(raw),featureData=featureData(raw))


	l = list(
		 uA.boxplot(raw.eset,path, is.log2 = TRUE,ylab = "Ct Value (undertermined set to 40)", xlab = "Sample", desc = "Boxplot Visualization of raw Ct Values") # Boxplots
		,uA.cordist.hclust(raw.eset,path, is.log2 = TRUE, xlab="Sample")
		,uA.pairwise.manhattan.hclust(raw.eset,path, is.log2 = TRUE,lab="Sample")# pairwise man heatmap
		,uA.mds(raw.eset,path, is.log2 = TRUE)# MDS
		,uA.pca(raw.eset,path, is.log2 = TRUE)# PCA
		)


	# Feature Categories
	fn = file.path(path,'ct_categories.pdf')
	dat = melt(lapply(featureCategory(raw),table))
	colnames(dat)[1] = "Category"
	p = qplot(data=dat,x=L1,y=value,geom="bar",stat='identity',fill=Category)+xlab("Sample")+
		ylab("Number")+ggtitle("Feature Categories")+coord_flip()
	pdf(fn,width=8.5,height=0.5*ncol(raw))
	print(p)
	dev.off()
	l = c(l,list(data.frame('Description'='Categories of Ct values, by sample','path'=fn)))


	index = do.call(rbind,l) # argh, annoying and unelegant to have to do this.. commitFigure(asap)
}








#' This function implements SOP quality controls for Affy Gene ST chipss
#'
#' @title SOP Quality Control Affymetrix Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.affyGeneST <- function(raw,path)
{

	Sys.setenv(R_THREADS=4)

	# Let's summarize without norm
	raw.eset = oligo::rma(raw,normalize=FALSE,background=FALSE)

	l = list(
		 uA.boxplot(raw.eset,path) # Boxplots
		,uA.cordist.hclust(raw.eset,path) # hclust
		,uA.pairwise.manhattan.hclust(raw.eset,path)# pairwise man heatmap
		,uA.mds(raw.eset,path)# MDS
		,uA.pca(raw.eset,path)# PCA
		)


	# More standard plots
	fn = file.path(path,'raw_values_densities.pdf')
	pdf(fn,height= 5, width=0.7 + 1 * ncol(raw)) #
	for(type in c("bg","all") ) #  c("pm","bg","all")
	{
		oligo::boxplot(raw,which=type,main=type)
		oligo::hist(raw,which=type,main=type)
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Distribution of raw values by probe type','path'=fn)))



	## Additional Affy-specific controls : AffyPLM
	Pset <- fitProbeLevelModel(raw, target='core')

	# Pseudo-images
	fn = file.path(path,'pseudo_images.pdf')
	pdf(fn,width=4,height=4)
	for(i in 1:length(colnames(Pset@chip.coefs)))
	{
		oligo::image(Pset,which=i,type='residuals'
			, main=paste0('PLM Residuals for\n ',colnames(Pset@chip.coefs)[i]))
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Chip Pseudo-Images of Residuals (oligo package)','path'=fn)))

	# RLE
	fn = file.path(path,'rle.pdf')
	pdf(fn,width=0.7 + 1.5 * length(colnames(Pset@chip.coefs)),height=5)
	oligo::RLE(Pset,main="RLE plot")
	dev.off()
	l = c(l,list(data.frame('Description'='Relative Log Expression (RLE) values (oligo package)','path'=fn)))

	# NUSE
	fn = file.path(path,'nuse.pdf')
	pdf(fn,width=0.7 + 1.5 * length(colnames(Pset@chip.coefs)),height=5)
	oligo::NUSE(Pset,main="NUSE plot")
	dev.off()
	l = c(l,list(data.frame('Description'='NUSE plot (oligo package)','path'=fn)))


	index = do.call(rbind,l)
}




#' This function implements SOP quality controls for Affy Clariom chipss
#'
#' @title SOP Quality Control Affymetrix Clariom Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F; modified by Eloi Mercier
#' @export
qualityControl.affyClariom <- function(raw,path)
{

	Sys.setenv(R_THREADS=4)

	# Let's summarize without norm
	raw.eset = oligo::rma(raw,normalize=FALSE,background=FALSE)

	l = list(
		 uA.boxplot(raw.eset,path) # Boxplots
		,uA.cordist.hclust(raw.eset,path) # hclust
		,uA.pairwise.manhattan.hclust(raw.eset,path)# pairwise man heatmap
		,uA.mds(raw.eset,path)# MDS
		,uA.pca(raw.eset,path)# PCA
		)


	# More standard plots
	fn = file.path(path,'raw_values_densities.pdf')
	pdf(fn,height= 5, width=0.7 + 1 * ncol(raw)) #
	type="all"
	oligo::boxplot(raw,which=type,main=type)
	oligo::hist(raw,which=type,main=type)
	dev.off()
	l = c(l,list(data.frame('Description'='Distribution of raw values by probe type','path'=fn)))



	## Additional Affy-specific controls : AffyPLM
	Pset <- fitProbeLevelModel(raw, target='core')

	# Pseudo-images
	fn = file.path(path,'pseudo_images.pdf')
	pdf(fn,width=4,height=4)
	for(i in 1:length(colnames(Pset@chip.coefs)))
	{
		oligo::image(Pset,which=i,type='residuals'
			, main=paste0('PLM Residuals for\n ',colnames(Pset@chip.coefs)[i]))
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Chip Pseudo-Images of Residuals (oligo package)','path'=fn)))

	# RLE
	fn = file.path(path,'rle.pdf')
	pdf(fn,width=0.7 + 1.5 * length(colnames(Pset@chip.coefs)),height=5)
	oligo::RLE(Pset,main="RLE plot")
	dev.off()
	l = c(l,list(data.frame('Description'='Relative Log Expression (RLE) values (oligo package)','path'=fn)))

	# NUSE
	fn = file.path(path,'nuse.pdf')
	pdf(fn,width=0.7 + 1.5 * length(colnames(Pset@chip.coefs)),height=5)
	oligo::NUSE(Pset,main="NUSE plot")
	dev.off()
	l = c(l,list(data.frame('Description'='NUSE plot (oligo package)','path'=fn)))


	index = do.call(rbind,l)
}












#' This function implements SOP quality controls. Note that additional figures can be genrated outside of the SOP and still be reported
#'
#' @title SOP Quality Control Illumina beadchip Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.illmnExpBeadChip <- function(raw,path)
{
	l = list(
		 uA.boxplot(raw,path) # Boxplots
		,uA.cordist.hclust(raw,path) # hclust
		,uA.pairwise.manhattan.hclust(raw,path)# pairwise man heatmap
		,uA.mds(raw,path)# MDS
		,uA.pca(raw,path)# PCA
		)

	# heatmap of control probes
	fn = file.path(path,'heatmap_control_probes.pdf')
	e = log2( raw$E )
	rownames(e) = make.unique(paste(raw$genes$Status,rownames(e)))
	e = e[raw$genes$Status!='regular',]
	pheatmap.wrapper(e,fn=fn,scale='none')
	l = c(l,list(data.frame('Description'='Heat map of intensities of control probes','path'=fn)))


	# NUmber of missing values table
	fn = file.path(path,'missing_values.html')
	mv = sort( apply(raw$E,MARGIN=2,function(co)
	{
		sum(  co==0 | is.na(co)   )
	}) )
	mv = data.frame('ArrayID'=names(mv),'Number of NAs or 0s'=mv,check.names=FALSE)
	writeLines(hwrite(mv,row.names=FALSE),fn)
	l = c(l,list(data.frame('Description'='Number of Missing Values','path'=fn)))

	# Chip images
	# ??
	# See ideas in Flexarray

	index = do.call(rbind,l) # argh, annoying and unelegant to have to do this.. commitFigure(asap)
}
# TODO: explore other pipelines e.g. lumi

#' This function implements SOP quality controls. Note that additional figures can be genrated outside of the SOP and still be reported
#'
#' @title SOP Quality Control Agilent Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.agilent2color <- function(raw,path)
{
	# path = 'data/qc'

	# Let's first make the unnorm, un back corr. M-values available
	rawMA = MA.RG(raw, bc.method="none")

	# Individual channel densities, per chip?
	l = list(
		 uA.boxplot(rawMA,path, fn='boxplot_mvals.pdf', main='M-Values (log Cy5/Cy3)', desc='Boxplots of log ratios; before normalization') # Boxplots
		,uA.cordist.hclust(rawMA,path,fn='cordist_hclust_mvals.pdf',main='M-Values (log Cy5/Cy3)',desc='Clustering of log ratios, based on the correlation distance; before normalization') # hclust
		,uA.pairwise.manhattan.hclust(rawMA,path, fn='pairwise_manhattan_mvals.pdf', main='M-Values (log Cy5/Cy3)', desc='Pairwise manhattan distance heat map, log ratios; before normalization')# pairwise man heatmap
		,uA.mds(rawMA,path, fn='mds_mvals.pdf', main='M-Values (log Cy5/Cy3)',desc='2D Multidimensional scaling plot (MDS), log ratios; before normalization')# MDS
		,uA.pca(rawMA,path, fn='pca_mvals.pdf', main='M-Values (log Cy5/Cy3)',desc='First two Principal Components (PCA), log ratios; before normalization')# PCA
		)


	# Densities for individual channels
	# All, individual arrays
	fn = file.path(path,'density_plots.pdf')
	pdf(fn,width=15,height=7)
	my.plotDensities(raw, main='All arrays')
	for(s in colnames(raw))
	{
		my.plotDensities(raw, arrays=s, main=s)
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Density plots, red and green channels','path'=fn)))
	# Densities of M-values... boxplot ok but uninformative sometimes.


	# heatmap of control probes
	fn = file.path(path,'heatmap_control_probes.pdf')
	e = rawMA[rawMA$genes$ControlType!=0,]
	e = avereps(e, ID = e$genes[["ProbeName"]])
	pheatmap.wrapper(e,fn=fn,scale='none')
	l = c(l,list(data.frame('Description'='Heat map of intensities of control probes','path'=fn)))

	# Chip images
	# > imageplot(log2(RG$Gb[,1]), RG$printer, low="white", high="green")
	fn = file.path(path,'raw_intensity_images.pdf')
	pdf(fn,width=5,height=5)
	for(s in colnames(raw))
	{

		mat = cbind(raw$genes[,c('Row','Col')],log2(raw$R[,s])) ; colnames(mat)[3] = 'value'
		mat = 	cast(mat,Row~Col,value='value')	; mat$Row=NULL
		mat[is.na(mat)] = 0; mat = mat[order(as.numeric(rownames(mat))),order(as.numeric(colnames(mat)))]
		mat = as.matrix(mat)
		image(mat,col=colorpanel(12, low='black', high='red'), main=paste('Red Channel',s,sep=' '))

		mat = cbind(raw$genes[,c('Row','Col')],log2(raw$G[,s])) ; colnames(mat)[3] = 'value'
		mat = 	cast(mat,Row~Col,value='value')	; mat$Row=NULL
		mat[is.na(mat)] = 0; mat = mat[order(as.numeric(rownames(mat))),order(as.numeric(colnames(mat)))]
		mat = as.matrix(mat)
		image(mat,col=colorpanel(12, low='black', high='green'), main=paste('Green Channel',s,sep=' '))


	}
	dev.off()
	l = c(l,list(data.frame('Description'='Images of Raw log2 Intensites, red and green channels','path'=fn)))
	# TODO: need to make sure color scale is ok...



	index = do.call(rbind,l) # argh, annoying and unelegant to have to do this.. commitFigure(asap)
}
# TODO: have the uA functions operate on esets and this function turn it into eset..
# code will be much much simpler










#' This function implements SOP quality controls for 450k. Only minfi QC for now.
#'
#' @title SOP Quality Control for Illumina 450k arrays'
#' @param raw mini RGChannel set object
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.450k <- function(raw,path)
{
	# path=file.path(db.path,'qc')
	l = list()

	# minfi QC report
	fn = file.path(path,'minfi_qcReport.pdf')
	minfi::qcReport(raw, sampNames=pData(raw)[['SampleID']], sampGroups=pData(raw)[['SampleID']],pdf=fn)
	l = c(l,list(data.frame('Description'='minfi standard QC report','path'=fn)))

	index = do.call(rbind,l) # argh, annoying and unelegant to have to do this.. commitFigure(asap)
}
# TODO: add more quality controls, inspired from other bioC packages such as lumi. Might even want to do the usual plots, mset = minfi::preprocessRaw....





#' This function implements SOP quality controls. Note that additional figures can be genrated outside of the SOP and still be reported
#'
#' @title SOP Quality Control Agilent Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.agilent <- function(raw,path)
{
	l = list(
		 uA.boxplot(raw,path) # Boxplots
		,uA.cordist.hclust(raw,path) # hclust
		,uA.pairwise.manhattan.hclust(raw,path)# pairwise man heatmap
		,uA.mds(raw,path)# MDS
		,uA.pca(raw,path)# PCA
		)

	# heatmap of control probes
	fn = file.path(path,'heatmap_control_probes.pdf')
	e = raw[raw$genes$ControlType!=0,]
	e <- backgroundCorrect(e, method="none")
	e <- normalizeBetweenArrays(e, method="none")
	e = avereps(e, ID = e$genes[["ProbeName"]])
	pheatmap.wrapper(e$E,fn=fn,scale='none')
	l = c(l,list(data.frame('Description'='Heat map of intensities of control probes','path'=fn)))


	# SingleSpotOutliers heatmap
	fn = file.path(path,'heatmap_grubbs_outliers.pdf')
	e = raw[ apply(raw[['other']][['SingleSpotOL']],MARGIN=1,sum )>0 , ]
	rownames(e) = make.unique(e$genes$ProbeName)
	e = ExpressionSet(log2(e$E))
	pheatmap.eset(e,file=fn,scale='none',which.rows=featureNames(e))
#	pheatmap.wrapper(log2(e$E),fn=fn,scale='none')
	l = c(l,list(data.frame('Description'=paste('Heat map of foreground intensities for the',nrow(e),'spots identified as having one outlying observation by a Grubbs test.'),'path'=fn)))

	# Chip images
	# > imageplot(log2(RG$Gb[,1]), RG$printer, low="white", high="green")
	if(!is.null(raw$printer)){
	fn = file.path(path,'raw_intensity_images.pdf')
	pdf(fn,width=5,height=5)
	for(s in colnames(raw))
	{
		imageplot(log2(raw$E[,s]), raw$printer, low="white", high="blue",main=s)
		imageplot(log2(raw$Eb[,s]), raw$printer, low="white", high="blue",main=paste0(s, ' (background)'))
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Images of Raw log2 Intensites','path'=fn)))
	}else{warning("raw object does not have slot raw$printer. Image plot skipped. This can be the case for custom arrays where not all spots are included. Would require added code to complete the matrix and add the printer object.")}


	index = do.call(rbind,l) # argh, annoying and unelegant to have to do this.. commitFigure(asap)
}
# TODO: have the uA functions operate on esets and this function turn it into eset..
# code will be much much simpler




#' This function implements SOP quality controls for Affy
#'
#' @title SOP Quality Control Affymetrix Expression Arrays'
#' @param raw raw data object, an ELIst.raw
#' @param path path to where the qc figures will be written
#' @author Lefebvre F
#' @export
qualityControl.affy <- function(raw,path)
{

	# Let's summarize without norm
	raw.eset = affy::rma(raw,normalize=FALSE,background=FALSE)

	l = list(
		 uA.boxplot(raw.eset,path) # Boxplots
		,uA.cordist.hclust(raw.eset,path) # hclust
		,uA.pairwise.manhattan.hclust(raw.eset,path)# pairwise man heatmap
		,uA.mds(raw.eset,path)# MDS
		,uA.pca(raw.eset,path)# PCA
		)


	## Additional Affy-specific controls : AffyPLM
	Pset <- fitPLM(raw)

	# Pseudo-images
	fn = file.path(path,'pseudo_images.pdf')
	pdf(fn,width=4,height=4)
	for(i in 1:length(sampleNames(Pset)))
	{
		image(Pset,which=i)
	}
	dev.off()
	l = c(l,list(data.frame('Description'='Chip Pseudo-Images (affyPLM package)','path'=fn)))


	# RLE
	fn = file.path(path,'rle.pdf')
	pdf(fn,width=0.7 + 1.5 * length(sampleNames(Pset)),height=4)
	RLE(Pset,main="RLE plot")
	dev.off()
	l = c(l,list(data.frame('Description'='Relative Log Expression (RLE) values (affyPLM package)','path'=fn)))


	index = do.call(rbind,l)
}
# TODO: there is really a plethora of possible affy QC.... leave it there for now!











