
#' dgCompareContrasts Returns a scatterplot (ggplot2 object) of the fold-changes of two contrasts. Text labels are added to the plot according to some p-value or fc thresholds.
#' 
#' @title Pairwise scatterplots of coefficient values
#' @param fitX A fit object that holds the contrast for the X,Y axis of the scatterplot. These object may well hold other contrasts. Shoud the compared contrasts be in the same fit object, simply subset it. Fit objects should have slot fit$genes populated a data.frame which has at least column "SYMBOL". Symbols ultimately used for on the plot are those from fitX$genes
#' @param fitY A fit object that holds the contrast for the X,Y axis of the scatterplot. These object may well hold other contrasts. Shoud the compared contrasts be in the same fit object, simply subset it. Fit objects should have slot fit$genes populated a data.frame which has at least column "SYMBOL". Symbols ultimately used for on the plot are those from fitX$genes
#' @param ctX Strings, the names of the contrasts as in colnames(fit)
#' @param ctY Strings, the names of the contrasts as in colnames(fit)
#' @param by.x Name of the column of fit$genes to use for joining. Duplicates are appendend ProbeID instead og beiing collapsed. This implies the need to collapse duplicates prior to joining. Defaults to Probe_Id (ILLXXXX), the nucl. sequence.
#' @param by.y Name of the column of fit$genes to use for joining. Duplicates are appendend ProbeID instead og beiing collapsed. This implies the need to collapse duplicates prior to joining. Defaults to Probe_Id (ILLXXXX), the nucl. sequence.
#' @param ctXY Interaction contrast. Not implemented
#' @param adjust.method p-value adjustment method. Defaults to BH.
#' @param p.value.X p-values and logFC cutoffs. They are used to determine for which gene labels (gene symbols) are added to the plot.
#' @param p.value.Y p-values and logFC cutoffs. They are used to determine for which gene labels (gene symbols) are added to the plot.
#' @param lfc.X p-values and logFC cutoffs. They are used to determine for which gene labels (gene symbols) are added to the plot.
#' @param lfc.Y p-values and logFC cutoffs. They are used to determine for which gene labels (gene symbols) are added to the plot.
#' @param main Title of the plot
#' @param xlab Alternative names for contrasts for prettier axis names.
#' @param ylab Alternative names for contrasts for prettier axis names.
#' @param xlim x,y limits. Default to -max,+max of logFCs. Ideally use identical limits to get a square plot.
#' @param ylim x,y limits. Default to -max,+max of logFCs. Ideally use identical limits to get a square plot.
#' @param asp The aspect ratio of the plot. Overrides xlim, ylim.
#' @param text.size numerical value to be passed to geom_text for the gene labels.
#' @param gene.color.groups A list of gene symbols vectors, eg. list("IFN"=c("STAT1","IRF7"),"RP"=c("RPS20","RSP19")).
#' @param gene.filter Gene list to subset the plot on.
#' @param smooth should a loess smooth be added to the plot. Defaults to TRUE
#' @param do.venn Should Vennerable objects be computed and appended to the output
#' @param outFile name for a png where to plot.
#' @return ggplot2 object.
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
dgCompareContrasts <- function(fitX,fitY,ctX = colnames(fitX)[1],
ctY = colnames(fitY)[1],by.x        = "Probe_Id",by.y        = by.x,
ctXY = NULL,adjust.method = "BH",p.value.X = 0.05,p.value.Y = p.value.X,
lfc.X = 0,lfc.Y = lfc.X,main = "Comparison",xlab = paste(ctX,' (log2 scale)',sep=""),
ylab = paste(ctY,' (log2 scale)',sep=""),xlim = NULL,ylim = xlim,asp=1,text.size = 1.5,
gene.color.groups = NULL,gene.filter=NULL,smooth      = TRUE,do.venn     = TRUE,outFile     = NULL)
{

   ## Checks
   if(! ctX %in% colnames(fitX))         {stop("ctX arugment is not a contrast in fitX.")}
   if(! ctY %in% colnames(fitY))         {stop("ctY argument is not a contrast in fitY.")}
   if(! by.x %in% colnames(fitX$genes) ) {stop("by.x arugment is not a column name if fitX$genes.")}
   if(! by.y %in% colnames(fitY$genes) ) {stop("by.y arugment is not a column name if fitY$genes.")}
 
   ## Prepare the data data.frame
   data =list( # This puts the info in fit in a more convenient, sortable format
     "X"=  data.frame(
       "logfc"=fitX$coefficients[,ctX]
       ,"fc"= sapply( fitX$coefficients[,ctX]  ,function(z) toFold(z))
       ,"p"=fitX$p.value[,ctX] # pavlue used in sorting required for collapsing
       ,"dt" = decideTests(fitX,adjust.method=adjust.method,lfc=lfc.X,p.value=p.value.X)[,ctX] # decidetest is later used for text label plotting decision
       ,"SYMBOL" = fitX$genes$SYMBOL
	,"ProbeID" = fitX$genes$ProbeID
       ), #,row.names="ProbeID"
     "Y"=  data.frame(
       "logfc"=fitY$coefficients[,ctY]
       ,"fc"= sapply( fitY$coefficients[,ctY]  ,function(z) toFold(z))
       ,"p"=fitY$p.value[,ctY]
       ,"dt" = decideTests(fitY,adjust.method=adjust.method,lfc=lfc.Y,p.value=p.value.Y)[,ctY]
       ,"SYMBOL" = fitY$genes$SYMBOL
	,"ProbeID" = fitY$genes$ProbeID
       )#,row.names="ProbeID"
     )
     # Define the joining columns
     data$X[[by.x]] =  fitX$genes[[by.x]]
     data$Y[[by.y]] =  fitY$genes[[by.y]]

     ## Make sure SYMBOL column is complete, order by p-value and collapse if necessary, print warning
     data = mapply(function(df,by)
      {
       	df = df[order(df$p,decreasing=FALSE),]	# Sort by p.value
	if(any(is.na(df$SYMBOL) | df$SYMBOL=="")) # Check for absent SYMBOLS
	{
		warning("Some elements of the SYMBOL column in fitX/Y are NA or empty. They are replaced by the corresponing ProbeID")
		df$SYMBOL[is.na(df$SYMBOL) | df$SYMBOL==""] =  df$ProbeID[is.na(df$SYMBOL) | df$SYMBOL==""] # just to make sure it is complete
	}
	if( anyDuplicated( df[[by]]) ) # Collpase if necessary
	{
        	df = df[ !duplicated( df[[by]]) ,]
		warning("There were duplicates in fitX/Y[[by.X/Y]] in terms of by.X/Y. Collapsed by the maximum p.value. Watch out for probe effect")
	}
        return(df)
      },data,list(by.x,by.y),SIMPLIFY=FALSE,USE.NAMES=TRUE )

      ## Join!
      print(paste("About to join the two datasets. "
		,length(intersect(data$X[[by.x]],data$Y[[by.y]]))," features are in common, "
		,length(setdiff(data$X[[by.x]],data$Y[[by.y]]))," are unique to X and "
		,length(setdiff(data$Y[[by.y]],data$X[[by.x]]))," are unique to Y.",sep="")  )
      colnames(data$X) = paste("X.",colnames(data$X),sep="")
      colnames(data$Y) = paste("Y.",colnames(data$Y),sep="")
      data = merge(data$X,data$Y,by.x=paste("X.",by.x,sep=""),by.y=paste("Y.",by.y,sep=""),all.x=FALSE,all.y=FALSE)
   
     ## Set SYMBOL as from fitX
     data$SYMBOL = data$X.SYMBOL
     
     
   ## Prepare color column
   if(!is.null(gene.color.groups))
     {
       print(paste(
		length(intersect(unlist(gene.color.groups,recursive=TRUE ),data$SYMBOL))
		," out of ",length(unique(unlist(gene.color.groups,recursive=TRUE )))
		," symbols from color groups map to SYMBOLs of the data.",sep=""))

       data$Group = "Others"
       overlapping = c()
       for(gr in names(gene.color.groups))
         {
           data$Group[ data$SYMBOL %in% gene.color.groups[[gr]] ] = gr
         }
	if(length(gene.color.groups)>=2)
	{
		overlapping = intersect(gene.color.groups[[1]], gene.color.groups[[2]])
		if(length(gene.color.groups)>2)
		{
			for(i in 3:length(gene.color.groups))
        	 	{
		   		overlapping = intersect(overlapping, gene.color.groups[[i]])
        	 	}
		}		
	}
	data$Group[ data$SYMBOL %in% overlapping ] = "Overlapping"
     }
   
   ## Now prepare the plot object
   data$Gene = data$SYMBOL ## These will be the text labels
	if(is.null(gene.filter)) # If text labels should be added to sig genes
	{
   		data$Gene[data$X.dt==0 & data$Y.dt==0 ]  = ""
	}else{
		data$Gene[! data$Gene %in% gene.filter]  = ""
	}# If text labels should to filter only

#	print(head(data))
   p =  ggplot(data=data,aes(x=X.logfc,y=Y.logfc))+  geom_point(aes(),alpha=0.1)+
     geom_abline(aes(),alpha=0.2,color='black')+geom_text(aes(label= Gene),alpha=0.75,size=text.size)+
      ggtitle(main)+xlab(xlab)+ylab(ylab)
   if(smooth){p=p+geom_smooth(aes(),linetype="dashed",alpha=0.5,color="blue")} ## Add or not the smooth
   if(!is.null(gene.color.groups)){p=p+aes(color=Group)+ scale_colour_brewer(pal = "Set1")}## Add color if applicable
  
 #  if (!is.na(asp)) 
    p <- p + theme(aspect.ratio = asp)
    if (!is.null(xlim)) {p <- p + xlim(xlim)}
    if (!is.null(ylim)) {p <- p + ylim(ylim)}


   ## Hijack ggplot2 object for venn diagram ?
   if(do.venn)
     {
       library(Vennerable)
       p$up   = list("X"=data$SYMBOL[data$X.dt ==  1] ,"Y"=data$SYMBOL[data$Y.dt ==  1]);names(p$up) =c(xlab,ylab)
       p$dwn  = list("X"=data$SYMBOL[data$X.dt == -1] ,"Y"=data$SYMBOL[data$Y.dt == -1]);names(p$dwn)=c(xlab,ylab)
       p$bth  = list("X"=data$SYMBOL[data$X.dt !=  0] ,"Y"=data$SYMBOL[data$Y.dt !=  0]);names(p$bth)=c(xlab,ylab)
       p$VennUp         = Venn(p$up)
       p$VennDown       = Venn(p$dwn)
       p$VennBoth       = Venn(p$bth)
     }


   ## Write to png
   if(!is.null(outFile)){
   png(file=outFile,width=960,height=960) 
   print(p)
   dev.off()
 }
   return(p)
}
## TODO :
##       - Improve coloring method. It sucks.
##       - find a way not to exlcude NAs, like probeID, RefSEq... but then cross-platform problem
##       - add support for not collapsing duplicate symbols
##       - Catch possible errors : too stringeant cutoff...
##       - Maybe merging shoulf be done on the bassis of somethig esle than gene....


