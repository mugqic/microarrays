

#' This function acomplished SOP preprocessing
#'
#' @title Preprocess
#' @param path path to where the skeleton of the db should be created
#' @author Lefebvre F
#' @export
preProcess <- function(obj,path=db.path,commit.eset=TRUE)
{

	pp.path = file.path(path,'preprocess')
	unlink(pp.path,recursive=TRUE,force=TRUE)# cleanup
	dir.create(pp.path,showWarnings=FALSE, recursive=TRUE)

	# Hard-coded mapping between pipelines and types
	types = c(
		 'AgilentGeneExpression'='agilent'
		,'Affy_miRNA'='affy'
		,'AgilentmiRNA'='agilentmiRNA'
		,'Affy3IVT'='affy'
		,'IlluminaHumanMethylation450'='450k'
		,'AgilentMeDIP'='agilentmedip'
		,'IlluminaExpressionBeadChip'='illmnExpBeadChip'
		,'AffyGeneST'='affyGeneST'
		,'taqman_miRNA' ="HTqPCR"
		,'AffyClariom'="affyClariom"
		)

	# Load the database content
	params   = getParams(path)
	pipeline = getSupportedPlatforms()[params[['platform.alias']],'pipeline']

	# Call the necessary method # TOODO: more advanced R with methods would be nice, alas
	if(types[[pipeline]] == "HTqPCR") # the use of eval abdirdges code, but loses flexibility, ie. no other args possible
	{
		l = preProcess.HTqPCR(obj, groupVars = params$dgea.variables)
	}else{
		l = eval(call(name=paste('preProcess',types[[pipeline]],sep='.'),obj))
	}



	eset = l[['obj']]
	description = l[['description']]

	# Re-annotate eset
	eset = annotateEset(eset,path)# re-annotate for Giggles

	# From now on the object is assumed to be an ExpressionSet. It should be re-annotated accordingly?
	#print(class(eset))

	# ComBat batch correction  (Assumed to be universal)
	if( !is.null( params[['batch.adjust.covariates']] ))
	{
		print(pData(eset)$batch)
		eset = ppComBat(eset,batch='batch',covariates=params[['batch.adjust.covariates']],data.dir=path)
		description = paste(description,' The ComBat batch adjustment method was applied with covariate(s) ',
				paste(params[['batch.adjust.covariates']],collapse=', '),'.',sep='')
	}


	# Averaging technical Replicates...
	if( !is.null( params[['avg.techreps.by']] ))
	{
		pData(eset)$.by = do.call(paste,pData(eset)[,  params[['avg.techreps.by']]   ,drop=FALSE])
		print(pData(eset)$.by)
		eset = ppAverageTechnicalReplicates(eset,by='.by')
		description = paste(description,' Technical replicate arrays were averaged by within the groups defined by variable(s) ',
				paste(params[['avg.techreps.by']],collapse=', '),'.',sep='')
	}


	if(commit.eset)
	{
		commitEset(eset,path=path)
	}

	# Save the eset as expression
	#write.csv(exprs(eset),file=file.path(pp.path,'expressions.csv'))

	# save the description
	save(description,file=file.path(pp.path,'description.RData'))

	return( eset )
}




#' SOP preprocessing of Agilent miRNA arrays with LVSmiRNA.
#'
#' Foreground intensities (median signal) are background subtracted, normexp backgorund
#' correction is applied with offset of 16, then quantile normalization + log2.
#' See Limma user's guide.
#'
#' @title preprocess Agilent expression
#' @param raw EListRaw object
#' @param bg.method c.f. backgroundCorrect()
#' @param offset c.f. backgroundCorrect()
#' @param norm.method c.f. normalizeBetweenArrays
#' @author Lefebvre F
#' @export
preProcess.agilentmiRNA <-function(raw)
{
	# # raw = loadRaw()
	# e               = raw[raw$genes[['ControlType']]=='0',] # remove controls
	# e               = normalizeBetweenArrays(e, method='none');e$E = 2**e$E # tirck to coerce to EList.. :) Ha!!
	# e$preprocessing = list('is.log'=c(FALSE)) # estVC expects this slot
	# e               = lvs(e,level="probe") # lvs default normalization
	# e = new('ExpressionSet',exprs=exprs(e))
	#
	# # Preprocessing functions should return a textual description of what they did.
	# description  = 'Normalization and summarization were accomplished through the use of the LVSmiRNA algorithm (PMID: 18318917). This algorithm identifies a subset of invariant miRNAs and uses them as a reference set for normalization. Features probing the same miRNA are summarized to yield a single expression value for each miRNA.'
	# return(list('obj'=e,'description'=description))

	preProcess.agilent(raw)

}




#' SOP preprocessing of qPCRset (taqman)
#'
#' RMA is applied
#' See oligo package.
#'
#' @title preprocess Agilent expression
#' @param raw qPCRset
#' @author Lefebvre F
#' @export
preProcess.HTqPCR <-function(obj , groupVars )
{
	# groupVars = c("Tissue") ; obj = raw

	min.group.size = min( table(   do.call(paste, pData(obj)[, groupVars ,drop=FALSE] )   ) )
	#min.group.size = 23 #### TEMP!!!
	controls = featureNames(obj)[ featureType(obj)=="control"]

	# Filter
	keep = apply(featureCategory(obj),1,function(ro){  sum(ro=="OK") >= min.group.size })
	obj = obj[keep,]
	message(sprintf("Filtered %s features with at least %s OK values out of a total of %s",sum(keep),min.group.size,length(keep)))

	# Impute and normalize
	pData(obj)$sampleName = sampleNames(obj) # stupid but qcrImpute requires sampleName (!)
	message("Imputing with nondetects package...")
	obj =  qpcrImpute(obj, groupVars = groupVars  )
	message("Normalizing with the deltaCt method...")
	obj =  normalizeCtData(obj,norm = "deltaCt", deltaCt.genes = controls )
				#scale.rank.samples, rank.type = "pseudo.median", Ct.max = 35, geo.mean.ref, verbose = TRUE)

	# Reverse to -detlaCt values
	exprs(obj) = -1 * exprs(obj)

	# Create pure ExpressionSet
	obj = ExpressionSet(exprs(obj))

	# Preprocessing functions should return a textual description of what they did.
	description  = sprintf(
		"Prepreprocessing started with a filtering of features with too many undetermined values. Specifically, %s features out of %s had at least %s OK values. This mimimum was set as the size of the smallest biological group.\
		We then proceeded to the imputation of undetermined observation, using the method of McCall et al. (PPMID: 24764462). This method was specifically developped for qPCR data where missing values can be failed reactions at random or low expression values censoring. This method was shown to perform better than a simple replacement by the number of cycles, which introduces serious biases in the downstream analyses.\
		Normalization was then performed by subtracting, for each sample, the average Ct value for the following control features: %s.\
		Finally the delta Ct values were multiplied by -1 to produce expression values that increase with abundance.",sum(keep),length(keep),min.group.size,paste(controls,collapse=', ')
		)
	return(list('obj'=obj,'description'=description))
}







#' SOP preprocessing of Affymetrix Gene ST arrays.
#'
#' RMA is applied
#' See oligo package.
#'
#' @title preprocess Agilent expression
#' @param raw EListRaw object
#' @author Lefebvre F
#' @export
preProcess.affyGeneST <-function(raw)
{

	e = oligo::rma(raw)


	# Preprocessing functions should return a textual description of what they did.
	description  = 'The Robust Multi-array Average (RMA) by Irizarry et al. was applied. This preprocessing method performs background adjutment using the rma convolution model, followed by quantile normalization and log2 transformation. Probes belonging to the same gene are then averaged using a robust model that estimates probe-specific effects using all arrays.'


	return(list('obj'=e,'description'=description))
}

#Eloi Mercier
preProcess.affyClariom = preProcess.affyGeneST








#' SOP preprocessing of Illumina BeadChip arrays.
#'
#' neqc is applied. Shi W, Oshlack A and Smyth GK (2010). Optimizing the noise versus
#' bias trade-off for Illumina Whole Genome Expression BeadChips.
#'
#' @title preprocess Illumina BeadChip expression
#' @param raw EListRaw object
#' @author Lefebvre F
#' @export
preProcess.illmnExpBeadChip <-function(raw)
{
	if(any(raw$E==0)){stop('Got 0 values.Need to implement imputation?')}
	if( ! 'Status' %in% colnames(raw$genes) ){stop('Column Status not found in gene annotation')}
	if( ! 'NEGATIVE' %in% raw$genes$Status ){stop('No negative control available? Did Nanuq change?')}
	warning('neqc will run on the following probe catalog:')
	print(sort(table(raw$genes$Status)))
	e = neqc(raw, status=raw$genes$Status, negctrl="NEGATIVE", regular="regular", offset=16
			,robust=FALSE, detection.p="Detection")

	# Set as expressionset
	e = new('ExpressionSet',exprs=e$E)

	# Attach detection P-values
	attr(e,'Detection') = raw$other$Detection[featureNames(e),]

	# Preprocessing functions should return a textual description of what they did.
	description  = 'The Illumina BeadChip is prone to missing intensity values. We impute them using KNN-imputation. We background correct and normalize the Illumina BeadChip platform using the neqc methodology by Oshlack et al. (2010) PMID: 20929874. This methodology performs the normexp background correction using negative control probes and quantile normalization using negative and positive control probes. Default parameters are used.'

	return(list('obj'=e,'description'=description))
}
# TODO: replace zeroes, wanring, impute, neqc
# TODO:  PP cite: Shi W, Oshlack A and Smyth GK (2010). Optimizing the noise versus
#     bias trade-off for Illumina Whole Genome Expression BeadChips.
 #    _Nucleic Acids Research_ 38, e204. <URL:
 #    http://nar.oxfordjournals.org/content/38/22/e204>
# Use lumi pipeline???
# Filter detection p-value??: could re-include them as not tested or P=1...??
















#' SOP preprocessing of Agilent MeDip arrays
#'
#' Foreground intensities (median signal) are background subtracted, normexp backgorund
#' correction is applied with offset of 16, then quantile normalization + log2.
#' See Limma user's guide.
#'
#' @title preprocess Agilent expression
#' @param raw EListRaw object
#' @param bg.method c.f. backgroundCorrect()
#' @param offset c.f. backgroundCorrect()
#' @param norm.method c.f. normalizeBetweenArrays
#' @author Lefebvre F
#' @export
preProcess.agilentmedip <-function(raw)
{
	MA <- normalizeWithinArrays(raw, method="none",bc.method='none', offset=0)
	MA <- normalizeBetweenArrays(MA,method="quantile")
	MA <- avereps(MA, ID=MA$genes[['ProbeName']])
	MA <- MA[MA$genes[['ControlType']]=='0',] # Remove control probes
	e  = new('ExpressionSet',exprs=MA$M)
	# Preprocessing functions should return a textual description of what they did.
	description  = 'Here, quantile normalization was performed on the log2(Cy5/Cy3) values. No background correction or within-array normalization was applied. Within-array duplicate spots were summarized by a simple average.'

	return(list('obj'=e,'description'=description))


}







#' SOP preprocessing of 450k arrays
#'
#' Minfi SWAN preprocessing
#'
#' @title preprocess 450k arrays
#' @param raw RGChannelSet object or ...
#' @author Lefebvre F
#' @export
preProcess.450k <-function(raw)
{
	m = minfi::preprocessSWAN(raw)

	e  = new('ExpressionSet',exprs=getM(m) ) # this extacts the m values

	# just attach the mset to the ExpressionSet... might be needed later
	attr(e,'MethylSet') = m

	# Preprocessing functions should return a textual description of what they did.
	description  = 'Pre-processing was done using SWAN normalization from the Bioconductor minfi package. See PMID: 22703947 for details.'

	return(list('obj'=e,'description'=description))


}










#' Averages Technical Replicates in an ExpressionSet
#'
#' @title Average Technical Replicates in an ExpressionSet
#' @param x ExpresionSet object
#' @param by What varLabels(x) should be used to collapse by averaging.
#' @return ExpressionSet object
#' @note Technical Replicates must be handled in some way. Possibilities include: remove them, collapse/average them or take them into account in the linear model (e.g. duplicateCorrelation). ppAverageTechnicalReplicates averages intensities of replicates. Often, technical replicates share the same 'tubeID' and therefore SampleID. Always take time to figure out what is the sampling unit of the experiment... very often the variable defining it will be a concatenation of the DonorID and the relevant phenotype factors.
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
ppAverageTechnicalReplicates <-function(x,by="SampleID")
{
	replicated = names(which(table(pData(x)[[by]])>1))
	for(sid in replicated)
	{
		aids =  sampleNames(x)[pData(x)[[by]]==sid]
		exprs(x)[,sampleNames(x)==aids[1] ] = apply(exprs(x[,sampleNames(x) %in% aids]),MARGIN=1,mean)
		x = x[, ! sampleNames(x) %in% aids[2:length(aids)] ] # remove others from the eset
	}
	return(x)
}


#' SOP preprocessing of Agilent expression arrays.
#'
#' Foreground intensities (median signal) are background subtracted, normexp backgorund
#' correction is applied with offset of 16, then quantile normalization + log2.
#' See Limma user's guide.
#'
#' @title preprocess Agilent expression
#' @param raw EListRaw object
#' @param bg.method c.f. backgroundCorrect()
#' @param offset c.f. backgroundCorrect()
#' @param norm.method c.f. normalizeBetweenArrays
#' @author Lefebvre F
#' @export
preProcess.agilent <-function(raw,bg.method='normexp',offset=16,norm.method='quantile')
{

	raw <- backgroundCorrect(raw, method=bg.method, offset=offset)
	raw <- normalizeBetweenArrays(raw, method=norm.method)
	raw <- avereps(raw, ID=raw$genes[['ProbeName']]) #;print(class(raw))		 # I am impressed... averages flags in raw$other!!!!
	raw <- raw[raw$genes[['ControlType']]=='0',] # Remove control probes

	e  = new('ExpressionSet',exprs=raw$E)

	if('other' %in% names(raw)){ # Here the strategy is to keep the flags.. but sum them over arrays to ultimately append to fdata
		flagSum =  sapply( raw$other, function(m)apply(m,MARGIN=1,sum)   )
		colnames(flagSum) = paste('FlagSum:',colnames(flagSum),sep=' ')
		attr(e,'flagSum') = flagSum
	}

	# Preprocessing functions should return a textual description of what they did.
	description  = 'The normexp method with an offset value of 16 was used for global background adjustment, followed by quantile normalization and a log2 transformation. Within-array duplicate spots were summarized by a simple average.'

	return(list('obj'=e,'description'=description))


}
	# Filter lowly expressed??
	#> neg95 <- apply(y$E[y$genes$ControlType==-1,],2,function(x) quantile(x,p=0.95))
	#> cutoff <- matrix(1.1*neg95,nrow(y),ncol(y),byrow=TRUE)
	#> isexpr <- rowSums(y$E > cutoff) >= 4
	#> table(isexpr)
	#isexpr
	#FALSE TRUE
	#11500 32754
	#Regular probes are code as \0" in the ControlType column:
	#> y0 <- y[y$genes$ControlType==0 & isexpr,]









#' SOP preprocessing of Affymetrix expression arrays.
#'
#' RMA is applied
#' See Limma user's guide.
#'
#' @title preprocess Agilent expression
#' @param raw EListRaw object
#' @param bg.method c.f. backgroundCorrect()
#' @param offset c.f. backgroundCorrect()
#' @param norm.method c.f. normalizeBetweenArrays
#' @author Lefebvre F
#' @export
preProcess.affy <-function(raw)
{

	e = affy::rma(raw)


	# Preprocessing functions should return a textual description of what they did.
	description  = 'The Robust Multi-array Average (RMA) by Irizarry et al. was applied. This preprocessing method performs background adjutment using the rma convolution model, followed by quantile normalization and log2 transformation. Probes belonging to the same probeset are then averaged using a robust model that estimates probe-specific effects using all arrays.'


	return(list('obj'=e,'description'=description))
}





